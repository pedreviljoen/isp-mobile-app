import React,{Component} from 'react';
import {ActivityIndicator, StyleSheet, View} from 'react-native'
import { PersistGate } from 'redux-persist/es/integration/react'
import { Provider } from 'react-redux';
import createStore from './src/lib/createStore';
import {persistStore} from 'redux-persist';
import  {createRootNavigator} from './src/navigators/SwitchNavigator';
import SplashScreen from 'react-native-splash-screen'

import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { compose } from 'redux'




export var configuredStore = configureStore({ firebase: {} });
var Intercom = require('react-native-intercom');

function configureStore(initialState){

    const store = createStore(initialState);
    let persistor = persistStore(store)
    // persistor.purge();

    return {persistor,store}
}


export default  class App extends Component {

    state: AppState = {
        ready: false,
        userSignedIn:false,
        profileCompleted:false,
    };

  
 
    async loadStaticResources(): Promise<void> {
        try {
            //await Images.downloadAsync();
        }
        catch(error) {
            // eslint-disable-next-line no-console
            console.error(error);
        }
    }

    componentDidMount(){

        const {userSignedIn,ready,profileCompleted} = this.state;

        if(!userSignedIn){

            configuredStore.store.firebase.auth().onAuthStateChanged((user)=> {
                if(user){
                    Intercom.registerIdentifiedUser({ userId: user.uid  });
                    configuredStore.store.firestore.get({ collection: 'users', doc: user.uid }).then( (data) => {
                        if(data._data != undefined){
                            this.setState({profileCompleted: false});
                            if(data._data.email !== null  && data._data.name  !== null && data._data.surname  !== null && data._data.country  !== null){
                                this.setState({profileCompleted: true});
                            }
                        }
                    }).then( () => {
                        this.setState({userSignedIn: true});
                        this.setState({ready: true});
                    }).catch((error) => {
                   });
                 } else{
                    this.setState({userSignedIn: false});
                    this.setState({ready: true});
                 }
            })
        }

    }


  render() {

        const {ready,userSignedIn,profileCompleted} = this.state;

        var Layout = createRootNavigator(userSignedIn,profileCompleted);
        if(ready)
        {
            SplashScreen.hide();
        }
      return (
          ready ?
             <Provider store={configuredStore.store}>
                <Layout/>
            </Provider>
              :
              <View style={{flex: 1,justifyContent: 'center', alignItems: "center"}}><ActivityIndicator size="large" color="#fd8343"/></View>
      )
  }
}



