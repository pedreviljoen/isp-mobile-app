/**
 * Created by rossd on 5/16/2018.
 */
import React,{Component} from 'react';
import {View,StyleSheet, Dimensions,Text,ActivityIndicator} from "react-native";
import {Button,Container,Header,Content,Footer,AppText,FadeInView} from "../components"
import  ThemeColors  from "../components/Theme";
var {height, width} = Dimensions.get('window');

export default class Welcome extends Component{

    state={
        isLoading:false,
    }


    render(){

        const {navigation} = this.props


        return(
        <View style={styles.container}>
                     <Text style={styles.welcomeTitle} ><Text>Welcome</Text></Text>
                     <Text style={styles.toHeader} ><Text>to</Text></Text>
                     <FadeInView>
                              <Text style={styles.welcomeHeader} ><Text>iSPANI</Text></Text>
                     </FadeInView>

                    {!this.state.isLoading?
                        <Button textColor={{color:"#fd8343"}} style={styles.signUpButton} 
                            onPress={()=>{
                                this.setState({
                                        isLoading:true,
                                })
                                setTimeout(function(){ 
                                    this.setState({
                                        isLoading:false,
                                    })
                                }.bind(this),);
                                navigation.navigate("Register")
                            }}>                                                                    
                            Lets Get Started
                         </Button>
                    :
                    <ActivityIndicator style={styles.signUpButton}  size="large" color={"#fdb48e"}/>
                    }
        </View>

        )

    }
}



const styles=StyleSheet.create({

    container:{
        backgroundColor:"#fd8343",        
        flex:1,
        flexDirection:"column",
        alignItems:"center",
      //  justifyContent:"center"
    },
    toHeader:{
        marginTop:height*0.005,
        marginBottom:height*0.004,
        color:"white",
        alignItems:"center",
        fontSize:height*0.04,
        fontWeight: '100',
        fontFamily: 'sans-serif-light'  
        
    },
    welcomeTitle:{
        marginTop:height*0.1,
        color:"white",
        alignItems:"center",
        fontSize:height*0.06,
        fontWeight: '100',
        fontFamily: 'sans-serif-light'  
    },
    welcomeHeader:{
        marginBottom:height*0.13,
        color:"white",
        alignItems:"center",
        fontSize:height*0.12,
        fontWeight: '700',
        
    },
    loginButton:{
        height:height*0.08,
        width:width*0.8,
        backgroundColor:"#fd8343",    
        borderColor:"#fff",
        borderWidth: 1,
        borderRadius:25,
        shadowColor: '#D3D3D3',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 1,
        shadowRadius: 3,
        elevation: 1,
    },
    signUpButton:{
        height:height*0.08,
        width:width*0.8,
        backgroundColor:"#fff",    
        borderRadius:25,
        shadowColor: '#D3D3D3',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 1,
        shadowRadius: 3,
        elevation: 1,
    }

})