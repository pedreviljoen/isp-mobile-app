/**
 * Created by xl on 10/05/2018.
 * Form Validation
 * for more information please check https://medium.com/@pavsidhu/validating-forms-in-react-native-7adc625c49cf
 */


import validation from './validation'

export default function validate(fieldName, value) {
  var validatejs = require("validate.js");

  var formValues = {}
  var formFields = {}

  formValues[fieldName] = value

  formFields[fieldName] = validation[fieldName]

  const result = validatejs(formValues, formFields)
 
  if (result) {
    return result[fieldName][0]
  }

  return null
}