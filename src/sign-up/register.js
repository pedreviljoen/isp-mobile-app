/**
 * Created by rossd on 5/16/2018.
 */
import React,{Component} from 'react';
import {View,StyleSheet,Text,KeyboardAvoidingView,ScrollView,Dimensions,ActivityIndicator} from "react-native";
import {BottomPageButton,Container,Header,Content,Footer,AppText,TextField,TextInput,BackButton} from "../components"
import CustomPhoneInput from '../components/CustomPhoneInput';
import AutoBind from "autobind-decorator";
import validate from './validateWrapper'
import  ThemeColors  from "../components/Theme";
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'



var {height, width} = Dimensions.get('window');

export class Register extends Component{


    state={
        spinnerText:"Please wait while we auto-verify the sms code",
        phoneNumber:"",
        phoneNumberError:"",
        isLoading:false,
        error:"",
        done:false
    }


    register = () =>{
        const {phoneNumber,error} = this.state
        const { firebase, auth , navigation, firestore, profile} = this.props;
        const phoneNumberError = validate('phoneNumber', phoneNumber)
        this.setState({
          phoneNumberError:phoneNumberError,
        })


        if (!phoneNumberError && !this.state.error) {
            this.setState({
                isLoading:true,
              })
              try {
                firebase.auth().signInWithPhoneNumber(phoneNumber)
                .then((confirmationResult) => {
                    if(confirmationResult){
                                firebase.auth().onAuthStateChanged((user) => {
                                    if (user) {

                                        let fbProfile = {};

                                        // bad assumption need to use the manual verification
                                        this.setState({
                                            done:true,
                                        }) 
                                        if(!isEmpty(user._auth)){
                                            firestore.get({ collection: 'users', doc: user.uid }).then( (data) => {
                                                if(data&&data._data !== undefined){
                                                        fbProfile ={
                                                            balance:data._data.balance && data._data.balance !== null ?data._data.balance:0,
                                                            avatarUrl:null,
                                                            country:data._data.country!== null?data._data.country:null,
                                                            name:data._data.name!==null?data._data.name:null,
                                                            surname: data._data.surname!==null?data._data.surname:null,
                                                            email:data._data.email!==null?data._data.email:null,
                                                            phoneNumber:phoneNumber,
                                                            role:"appUser"
                                                        }
                                                }
                                                else{
                                                    fbProfile ={
                                                        balance:0,
                                                        country:null,
                                                        name:null,
                                                        surname:null,
                                                        email:null,
                                                        phoneNumber:phoneNumber,
                                                        role:"appUser"
                                                    }
                                                }

                                                firebase.updateProfile(fbProfile)
                                            }).catch((error) => {
                                           });
                                        }
                                        else{
                                            setTimeout(() =>{
                                                if(!this.state.error&&!this.state.done){
                                                    if(isEmpty(auth)){
                                                        this.setState({
                                                            spinnerText:"auto-verification is delaying, we are redirecting you to verify manually"
                                                        }) 
                                                        setTimeout(() =>{
                                                            navigation.navigate("VerifyCode", {
                                                                confirmationResult:confirmationResult,
                                                                phoneNumber:phoneNumber 
                                                            });
                                                        }, 4000);
                                                    }
                                                }
                                            }, 10000);
                                        }
                                    }
                                    else{
                                        setTimeout(() =>{
                                            if(!this.state.error&&!this.state.done){
                                                if(isEmpty(auth)){
                                                    this.setState({
                                                        spinnerText:"auto-verification is delaying, we are redirecting you to verify manually"
                                                    }) 
                                                    setTimeout(() =>{
                                                        navigation.navigate("VerifyCode", {
                                                            confirmationResult:confirmationResult, 
                                                            phoneNumber:phoneNumber 
                                                        });
                                                    }, 4000);

                                                }
                                            }

                                        }, 10000);
                                    }
                                });
                    }
                    else{
                        this.setState({
                            error:"Something went wrong please try again later",
                            isLoading:false,
                        })
                    }
                })
                .catch((error) => {
                    this.setState({
                        error:error.message,
                        isLoading:false,
                    })
                });
            }
            catch(err) {
                this.setState({
                    error:err.message,
                    isLoading:false,
                })
            }
        }
    }
    //TODO: move helper functions to a separate folder
    isThereAnError(errorText){
        if(typeof(errorText) == "undefined" || errorText == null || errorText == "" ){
           return false;
        }
        return true;
    }



    onChangeTextHandle = (errorText,value,errorName,name) =>{

        this.setState({
            error:"",
          })

        const {phoneNumber} = this.state
        this.setState({[name]: value.trim()})

        if(!this.isThereAnError(errorText)){
            this.setState({
                [errorName] : validate(name,this.state[name])
            })

        }
    }



    render(){

       const { navigation } = this.props;

        return(
                !this.state.isLoading ?
                <Container>
                      <View style={{marginLeft:width*0.05,marginTop:width*0.05}}>
                          <BackButton {...{navigation}} color={"#696969"} />
                      </View>
                    <View>
                        <View  style={styles.header}>
                            <Text style={styles.registerTitle}>
                                Register Your Account
                            </Text>
                            <Text style={styles.registerSubText}>
                                Get your own iSpani account by entering your cellphone number and we will auto-verify with an sms.
                            </Text>
                        </View>
                    </View>
                    <Content style={styles.content}>
                        <View style={styles.removeBorder}>
                                    <CustomPhoneInput
                                        style={{height:height*0.2}}
                                        ref='phone'
                                        onChangePhoneNumber={number =>  this.onChangeTextHandle(validate('phoneNumber', this.state.phoneNumber),number,'phoneNumberError','phoneNumber')}
                                        onBlur={() => {
                                                this.setState({
                                                    phoneNumberError: validate('phoneNumber', this.state.phoneNumber)
                                                })
                                            }}
                                        value={this.state.phoneNumber}
                                        error={this.state.phoneNumberError}
                                        label={"CELLPHONE NUMBER"}
                                        description={""}
                                        onChangeText={value =>  this.onChangeTextHandle(validate('phoneNumber', this.state.phoneNumber),value,'phoneNumberError','phoneNumber')}
                                        />

                                    </View>
                                        <View  style={ styles.removeBorder}>
                                            {this.state.error ?
                                            <Text style={[ThemeColors.errorColor,{ width: width * .8 },{ marginLeft: width * .1 }]} >{this.state.error }</Text>: null
                                            }
                                        </View>

                        </Content>
                        <Footer>
                            <View style={styles.loginSection}>
                                <BottomPageButton loading={this.state.isLoading} text="SIGN UP" onPress={this.register} />
                            </View>
                        </Footer>
                </Container>:
                <View style={{flex: 1,justifyContent: 'center', alignItems: "center"}}>
                        <ActivityIndicator size="large" color="#fd8343"/>
                        <Text style={styles.verifyText}>
                               {this.state.spinnerText}
                        </Text>
                </View>

        )

    }

}

const styles=StyleSheet.create({

    registerTitle:{
        color:"#fd8343",
        alignItems:"center",
        fontSize:height*0.045,
        fontWeight: '100',
        fontFamily: 'sans-serif-light'  
    },
    registerSubText:{
        color:"rgba(253, 131, 67,0.8)",
        alignItems:"center",
        fontSize:height*0.025,
        fontWeight: '100',
        fontFamily: 'sans-serif-light',
        paddingTop:30,
        textAlign:"justify",
        width:width*0.8
    },
    verifyText:{
        paddingTop:height*0.02,
        fontSize:height*0.025,
        fontWeight: '100',
        width:width*0.8,
        fontFamily: 'sans-serif-light',
        textAlign:"center"  
    },
    registerQuestionTitle:{
        color:"black",
        alignItems:"center",
        fontSize:height*0.04,
        fontWeight: '100',
        fontFamily: 'sans-serif-light'  ,
        paddingTop:height*0.02,
    },

    header:{
        paddingTop:height*0.05,
        paddingLeft:width*0.1,
         height:height*0.1,
     },

    buttonBar:{
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center"
    },
    registerHeader:{
        justifyContent:"center",
        alignItems:"center"
    },
    content:{
        justifyContent:"center",
        alignItems:"center"
    },
    removeBorder:{
        borderColor:'transparent',
    },
    loginButton:{
        backgroundColor:'rgba(0,0,0,0.9)'
    }
})

export default compose(
    firebaseConnect(),
    firestoreConnect(),
    connect(({ firebase: { auth , profile } }, props) => ({ auth, profile })),
    connect(({ firestore: { ordered } }, props) => ({users: ordered.users})),
  )(Register)

