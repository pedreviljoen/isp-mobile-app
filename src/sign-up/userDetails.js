/**
 * Created by rossd on 5/16/2018.
 */
import React,{Component} from 'react';
import {View,StyleSheet,KeyboardAvoidingView,ScrollView,Dimensions,AppRegistry,Text,StatusBarIOS,PixelRatio,ActivityIndicator} from "react-native";
import {BackButton,CountryTextField,BottomPageButton,Container,Header,Content,Footer,AppText,TextField,TextInput} from "../components"

import AutoBind from "autobind-decorator";
import validate from './validateWrapper'
import  ThemeColors  from "../components/Theme";

import CountryPicker, {getAllCountries} from 'react-native-country-picker-modal'

import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'

const SOUTH_AFRICA = [];
var {height, width} = Dimensions.get('window');



export class UserDetails extends Component{
 

    state={
        country:"South Africa",
        countryError:"",
        email:this.props.profile.email,
        emailError:"",
        firstName:this.props.profile.name,
        firstNameError:"",
        lastName:this.props.profile.surname,
        lastNameError:"",
        isLoading: false,
        error: "",
        cca2: 'ZA',
        spinnerText:"Please wait while we update your details"
    }

    
    finishSignUp = () => {

        
        const { firebase, auth , navigation, firestore, profile} = this.props;

      

        const {country,email,firstName,lastName} = this.state
        const countryError = validate('names',  country)
        const firstNameError = validate('names', firstName)
        const lastNameError = validate('names', lastName)
        const emailError = validate('username', email)
    
        this.setState({
          countryError: countryError,
          emailError:emailError,
          firstNameError:firstNameError,
          lastNameError:lastNameError,
        })



        if (!emailError && !countryError && !firstNameError &&!lastNameError ) {   
                                
                                this.setState({
                                    isLoading:true,
                                })

                                firebase.auth().onAuthStateChanged((user) => {
                                    if (user) {

                                        var currentTime = new Date().getTime()
                                        let fbProfile ={
                                            balance:profile.balance && profile.balance !== null?profile.balance:0,
                                            avatarUrl:profile.avatarUrl?profile.avatarUrl:null,
                                            country:country,
                                            email:email,
                                            name:firstName,
                                            surname:lastName,
                                            phoneNumber:auth.phoneNumber,
                                            role:profile.role

                                        }  

                                        let userProfile ={
                                            displayName:firstName+" "+lastName,
                                            email:email,
                                            photoUrl:"",
                                        }  
                                        
                                        firebase.updateProfile(fbProfile).then(() => {
                                            user.updateProfile(userProfile).then(() => {
                                                navigation.navigate("SignedIn");
                                              }).catch((error) => {
                                                this.setState({
                                                    error:error.message,
                                                    isLoading:false,
                                                })  
                                              });
                                          }).catch((error) => {
                                            this.setState({
                                                error:error.message,
                                                isLoading:false,
                                            })  
                                          });

                                     }                    
                                }); 
            }
    }
    //TODO: move helper functions to a separate folder
    isThereAnError(errorText){
        if(typeof(errorText) == "undefined" || errorText == null || errorText == "" ){
           return false;
        }
        return true;
    }
     //TODO: move helper functions to a separate folder

    onChangeTextHandle = (errorText,value,errorName,name) => {

        const {country,email,firstName,lastName} = this.state

        this.setState({
            error:"",
          })


        this.setState({[name]: value.trim()})

        if(!this.isThereAnError(errorText)){

            this.setState({
                [errorName] : validate(name,this.state[name])
            }) 

        }    
    }
    render(){
        const { navigation,profile,auth} = this.props;

        return(
            !this.state.isLoading?
            <Container>
                    <View style={{marginLeft:width*0.05,marginTop:width*0.1}}>
                          {/* <BackButton {...{navigation}} color={"#696969"} /> */}
                      </View>  
                    <View>
                        <View  style={styles.header}>
                            <Text style={styles.registerTitle}>
                            Please enter your details below
                            </Text>
                        </View>
                    </View>

                <Content style={styles.content}>
                        <View style={ styles.removeBorder}>
                                    <TextField  
                                        autoFocus={true}
                                        autoCapitalize={"sentences"}
                                        secureTextEntry={false}
                                        returnKeyType={"next"}
                                        keyboardType={"default"}
                                        onChangeText={value =>  this.onChangeTextHandle(validate('names', this.state.firstName),value,'firstNameError','firstName')}
                                        onBlur={() => {
                                            this.setState({
                                                firstNameError: validate('names', this.state.firstName)
                                            })
                                        }}
                                        value={this.state.firstName}
                                        error={this.state.firstNameError}
                                        label={"FIRSTNAME"}/>

                                </View> 
                                <View style={ styles.removeBorder}>
                                    <TextField  
                                        autoCapitalize={"sentences"}
                                        secureTextEntry={false}
                                        returnKeyType={"next"}
                                        keyboardType={"default"}
                                        onChangeText={value =>  this.onChangeTextHandle(validate('names', this.state.lastName),value,'lastNameError','lastName')}
                                        onBlur={() => {
                                            this.setState({
                                                lastNameError: validate('names', this.state.lastName)
                                            })
                                        }}
                                        value={this.state.lastName}
                                        error={this.state.lastNameError}
                                        label={"SURNAME"}/>

                                </View> 
                                <View style={[styles.removeBorder,{marginTop:height*0.01}]}>
                                    <TextField 
                                        autoCapitalize={"none"}
                                        secureTextEntry={false}
                                        returnKeyType={"next"}
                                        keyboardType={"email-address"}
                                        onChangeText={value =>  this.onChangeTextHandle(validate('username', this.state.email),value,'emailError','email')}
                                        onBlur={() => {
                                            this.setState({
                                            emailError: validate('username', this.state.email)
                                            })
                                        }}
                                        value={this.state.email}
                                        error={this.state.emailError}
                                        label={"EMAIL ADDRESS"}/>

                                </View> 
                                <View style={ styles.removeBorder}>
                                        <View style={[styles.textErrorInputViewStyles,{flexDirection:"row"},styles.removeBorder]}>
                                            <View style={{marginBottom:height*0.01}}>
                                                { typeof(this.state.countryError) == "undefined" || this.state.countryError == null || this.state.countryError == "" ?
                                                        <Text style={[styles.bigBlack,{textAlign:"left"}]}>COUNTRY<Text style={{ fontSize:15,fontFamily: 'sans-serif-light',color:"#939393"}}> (press flag)</Text></Text>
                                                        :
                                                        <Text style={[styles.bigBlack, ThemeColors.errorColor,{textAlign:"left"} ]}>COUNTRY</Text>
                                                }
                                            </View>
                                        </View>
                                        <View  style={[styles.textInputViewStyles,{flexDirection:"row"},styles.removeBorder]}>
                                            <View  style={{marginTop:7,marginRight:6}}> 
                                                    <CountryPicker
                                                    onChange={(value)=>{
                                                        this.onChangeTextHandle(validate('names', this.state.country),value.name,'countryError','country')
                                                        this.setState({
                                                            cca2: value.cca2,
                                                        })
                                                        }}
                                                    cca2={this.state.cca2}
                                                    translation='eng'
                                                    />
                                            </View>
                                            <View > 
                                                    <Text style={[styles.textInputStyles,{marginTop:10}]}>{this.state.country}</Text>
                                            </View>
                                        </View>

                                    </View> 
                                    <View  style={ styles.removeBorder}> 
                                        {this.state.error ? 
                                        <Text style={[ThemeColors.errorColor,{ width: width * .8 },{ marginLeft: width * .1 }]} >{this.state.error}</Text>: null
                                        }
                                    </View> 

            
      
                </Content>
                <Footer>
                    <View style={styles.loginSection}>
                        <BottomPageButton loading={this.state.isLoading}  text="Next" onPress={this.finishSignUp} />
                    </View>
                </Footer>
            </Container>:
            <View style={{flex: 1,justifyContent: 'center', alignItems: "center"}}>
                        <ActivityIndicator size="large" color="#fd8343"/>
                        <Text style={styles.verifyText}>
                               {this.state.spinnerText}
                        </Text>
            </View>

        )

    }

}

const styles=StyleSheet.create({

    textInputStyles:{
        color:"#939393",
        fontSize:15,
        backgroundColor:"#d3d3d3",
        height:40,
        paddingLeft:15,
        marginBottom:10,
        width: width * .8,
        fontFamily: 'sans-serif-light',
    }, 
    
    bigBlack: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 15,
        fontFamily: 'sans-serif-light',

    }, 
    textInputViewStyles:{
        height:height*.075,
        paddingLeft:15,
        marginBottom:10,
        width: width * .8,
        backgroundColor:"#d3d3d3",
        borderRadius:5

    }, 
    textErrorInputViewStyles:{
        width: width * .8,
    },
    registerTitle:{
        color:"#fd8343",
        alignItems:"center",
        fontSize:height*0.04,
        fontWeight: '100',
        fontFamily: 'sans-serif-light', 
        marginRight:width*0.05
    },
    verifyText:{
        paddingTop:height*0.02,
        fontSize:height*0.025,
        fontWeight: '100',
        width:width*0.8,
        fontFamily: 'sans-serif-light',
        textAlign:"center"    
    },
    registerQuestionTitle:{
        color:"black",
        alignItems:"center",
        fontSize:height*0.04,
        fontWeight: '100',
        fontFamily: 'sans-serif-light'  ,
        paddingTop:height*0.01,
    },

    header:{
        paddingTop:height*0.01,
        paddingLeft:width*0.1,
         height:height*0.1,
         marginBottom:height*0.03,
     },

    buttonBar:{
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center"
    },
    registerHeader:{
        justifyContent:"center",
        alignItems:"center"
    },
    content:{
        justifyContent:"center",
        alignItems:"center"
    },
    removeBorder:{
        borderColor:'transparent',
    },
    loginButton:{
        backgroundColor:'rgba(0,0,0,0.9)'
    }
})

export default compose(
    firebaseConnect(),
    firestoreConnect(),
    connect(({ firebase: { auth , profile } }, props) => ({ auth , profile })),
    connect(({ firestore: { ordered } }, props) => ({users: ordered.users})),
  )(UserDetails);

