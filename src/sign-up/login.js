/**
 * Created by rossd on 5/16/2018.
 */
import React,{Component} from 'react';
import {View,StyleSheet,Text,KeyboardAvoidingView,ScrollView,Dimensions,ActivityIndicator} from "react-native";
import {BottomPageButton,Container,Header,Content,Footer,AppText,TextField} from "../components"
import CustomPhoneInput from '../components/CustomPhoneInput';
import AutoBind from "autobind-decorator";
import validate from './validateWrapper'
import  {ThemeColors}  from "../components/Theme";
import { withFirebase } from 'react-redux-firebase'

var {height, width} = Dimensions.get('window');
export class Login extends Component{


    state={
        phoneNumber:"",
        phoneNumberError:"",
        password:"",
        passwordError:"",
        isLoading: false,
        error: {message:""}
    }

    login = () => {
        const {phoneNumber,password} = this.state
        const phoneNumberError = validate('phoneNumber', this.state.phoneNumber)
        const passwordError = validate('password', this.state.password)
        this.setState({
            phoneNumberError: phoneNumberError,
            passwordError: passwordError
        })

        if (!phoneNumberError && !passwordError) {
         this.props.login(this.state.phoneNumber,this.state.password);
        }
    }

    //TODO: move helper functions to a separate folder
    isThereAnError(errorText){
        if(typeof(errorText) == "undefined" || errorText == null || errorText == "" ){
           return false;
        }
        return true;
    }

    //TODO: move helper functions to a separate folder
    onChangeTextHandle = (errorText,value,errorName,name) => {

        this.setState({[name]: value.trim()})

        if(!this.isThereAnError(errorText)){

            this.setState({
                [errorName] : validate(name,this.state[name])
            }) 
        }
    }

    render(){
        const {isLoading,error} = this.props
        return(
            <Container>
                <Header>
                    <View style={styles.loginHeader}>
                        <AppText h3>
                            Enter your details below
                        </AppText>
                    </View>
                </Header>
                <Content style={styles.content}>
                    <View style={styles.removeBorder}>
                                     <CustomPhoneInput
                                      ref='phone'
                                      onChangePhoneNumber={number =>  this.onChangeTextHandle(validate('phoneNumber', this.state.phoneNumber),number,'phoneNumberError','phoneNumber')}
                                      onBlur={() => {
                                            this.setState({
                                                phoneNumberError: validate('phoneNumber', this.state.phoneNumber)
                                            })
                                        }}
                                      value={this.state.phoneNumber}
                                      error={this.state.phoneNumberError}
                                      label={"PHONE NUMBER"}
                                      description={"Your Cell Number"}
                                      onChangeText={value =>  this.onChangeTextHandle(validate('phoneNumber', this.state.phoneNumber),value,'phoneNumberError','phoneNumber')}
                                      />
                                </View>

                        <View style={ styles.removeBorder} >
                            <TextField
                                        autoCapitalize={"none"}
                                        secureTextEntry={true}
                                        onChangeText={value =>  this.onChangeTextHandle(validate('password', this.state.password),value,'passwordError','password')}
                                        onBlur={() => {
                                            this.setState({
                                            passwordError: validate('password', this.state.password)
                                            })
                                        }}
                                        label={"PASSWORD"}
                                        error={this.state.passwordError}
                                        />
                        </View>
                        <View  style={ styles.removeBorder}>
                            {error ?
                            <Text style={[ThemeColors.errorColor,{ width: width * .8 },{ marginLeft: width * .1}]} >{ error.message}</Text> : null
                            }
                        </View>


                </Content>
                <Footer>
                    <View style={styles.loginSection}>
                                {!isLoading ?
                                    <BottomPageButton  text="Login" onPress={this.login}/>
                                    :
                                    <ActivityIndicator size="large" color={ThemeColors.primaryColor} />
                                }
                    </View>


                </Footer>
            </Container>
        )
    }
}




const styles = StyleSheet.create({
    buttonBar:{
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center"
    },
    registerHeader:{
        justifyContent:"center",
        alignItems:"center"
    },
    content:{
        justifyContent:"center",
        alignItems:"center"
    },
    removeBorder:{
        borderColor:'transparent',
    },
    loginButton:{
        backgroundColor:'rgba(0,0,0,0.9)'
    }
})


export default withFirebase(Login);

