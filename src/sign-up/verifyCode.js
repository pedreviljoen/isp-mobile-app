/**
 * Created by rossd on 5/16/2018.
 */
import React,{Component} from 'react';
import {View,StyleSheet,Text,KeyboardAvoidingView,ScrollView,Dimensions,ActivityIndicator} from "react-native";
import {BackButton,BottomPageButton,Container,Header,Content,Footer,AppText,TextField,TextInput} from "../components"
import AutoBind from "autobind-decorator";
import validate from './validateWrapper'
import ThemeColors  from "../components/Theme";

import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'




var {height, width} = Dimensions.get('window');

export class VerifyCode extends Component{


    state={
        spinnerText:"Please wait while we verify the sms code",
        code:"",
        isLoading: false,
        codeError: ""
    }


    verifyCodeHandle = () => {
    
         const {navigation,firebase,auth} = this.props
         const  confirmationResult = navigation.getParam('confirmationResult', null);
         const phoneNumber = navigation.getParam('phoneNumber', null);
         if(this.state.code && this.state.code!==""){
            this.setState({
                isLoading:true,
              })
            confirmationResult.confirm(this.state.code)
            .then((user) =>{
                   firebase.auth().onAuthStateChanged((user) => {
   
                       if(user){
                           let fbProfile ={
                               balance:0,
                               avatarUrl:null,
                               country:null,
                               name:null,
                               surname:null,
                               email:null,                                   
                               phoneNumber:phoneNumber,
                               role:"appUser"                               
                           }
           
                           firebase.updateProfile(fbProfile).then(() => {
                               if(isEmpty(auth)){
                                this.setState({
                                    isLoading:false,
                                }) 
                               }
                           
                           }).catch((error) => {
                               this.setState({
                                   error:error.message,
                                   isLoading:false,
                               })  
                           });
           
                       }
   
                   })
   
            }  )
            .catch((error) =>{
               this.setState({
                   error:error.message,
                   isLoading:false,
               }) 
            });
         }
         else{
            this.setState({
                error:"Please enter your code",
                isLoading:false,
            }) 
         }
                                
    }
    
    setCodeState = (value) => { 
        this.setState({ 
            code:value,
            error:"",
        }) 
    }

    render(){

        const { navigation } = this.props;

        return(
            !this.state.isLoading ?
            <Container>
                  <View style={{marginLeft:width*0.05,marginTop:width*0.05,marginRight:width*0.05}}>
                          <BackButton {...{navigation}} color={"#696969"} />
                 </View>
                 <View>
                     <View  style={styles.header}>
                            <Text style={styles.registerTitle}>
                                Time To Verify Your Code
                            </Text>
                        </View>
                    </View>
                <Content style={styles.content}>
                        <View style={[ styles.removeBorder, styles.textInputContainer]} >
                            <TextField  
                                autoCapitalize={"none"}
                                secureTextEntry={true}
                                returnKeyType={"next"}
                                keyboardType={"phone-pad"}
                                onChangeText={value =>  this.setCodeState(value)}
                                label={"VERIFY YOUR CODE"}
                                error={this.state.codeError}
                            />
                        </View>
                        <View  style={styles.removeBorder}> 
                            {this.state.error ? 
                            <Text style={[ThemeColors.errorColor,{ width: width * .8 },{ marginLeft: width * .1 }]} >{this.state.error}</Text>: null
                            }
                        </View> 
            </Content>
                <Footer>
                    <View style={styles.loginSection}>
                        <BottomPageButton loading={this.state.isLoading}  text="Next" onPress={this.verifyCodeHandle} />
                    </View>
                </Footer>
            </Container>
            :
            <View style={{flex: 1,justifyContent: 'center', alignItems: "center"}}>
                        <ActivityIndicator size="large" color="#fd8343"/>
                        <Text style={styles.verifyText}>
                               {this.state.spinnerText}
                        </Text>
            </View>

        )

    }

}

const styles=StyleSheet.create({

    registerTitle:{
        color:"#fd8343",
        alignItems:"center",
        fontSize:height*0.045,
        fontWeight: '100',
        fontFamily: 'sans-serif-light', 
        marginRight:width*0.05
    },
    verifyText:{
        paddingTop:height*0.02,
        fontSize:height*0.025,
        fontWeight: '100',
        width:width*0.8,
        fontFamily: 'sans-serif-light',
        textAlign:"center"  
    },
    registerQuestionTitle:{
        color:"black",
        alignItems:"center",
        fontSize:height*0.04,
        fontWeight: '100',
        fontFamily: 'sans-serif-light'  ,
        paddingTop:height*0.02,
    },

    header:{
        paddingTop:height*0.05,
        paddingLeft:width*0.1,
         height:height*0.1,
     },

    buttonBar:{
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center"
    },
    registerHeader:{
        justifyContent:"center",
        alignItems:"center"
    },
    registerContent:{
      justifyContent:"center",
      alignItems:"center"
    },
    loginSection:{
        alignItems:"center"
    },
    content:{
        flex:1,
        justifyContent:"center",
        alignItems:"center"
    },
    textInputContainer:{
    },
    removeBorder:{
        borderColor:'transparent',
    },
    loginButton:{
        backgroundColor:'rgba(0,0,0,0.9)'
    }
})

export default compose(
    firebaseConnect(),
    connect(({ firebase: { auth , profile } }, props) => ({ auth, profile })),
  )(VerifyCode);


