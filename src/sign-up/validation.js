/**
 * Created by xl on 10/05/2018.
 * Form Validation
 * for more information please check https://medium.com/@pavsidhu/validating-forms-in-react-native-7adc625c49cf
 */

const validation = {
    username: {
      presence: {
        allowEmpty: false,
        message: '^Please enter an email address'
      },
      email: {
        message: '^Please enter a valid email address'
      }
    },
    
    password: {
      presence: {
        allowEmpty: false,
        message: '^Please enter a password'
      },
      length: {
        minimum: 6,
        message: '^Your password must be at least 6 characters'
      }
    },

    names: {
      presence: {
        allowEmpty: false,
        message: '^Please enter a name'
      },
      length: {
        minimum: 1,
        message: '^Please enter a valid name'
      }
    },
    answers: {
      presence: {
        allowEmpty: false,
        message: '^Please enter an answer'
      },
      length: {
        minimum: 1,
        message: '^Please enter an answer'
      }
    },
    number: {
      presence: {
        noStrings:true,
        allowEmpty: false,
        message: '^Please enter a number'
      },
    },
    phoneNumber: {
      presence: {
        allowEmpty: false,
        message: '^Please enter a phone number'
      },
      length: {
        minimum: 12,
        message: '^Please enter a valid phone number'
      },
      format: {
        pattern: "^((?:\\+))(\\d{10,13})$",
        message: "^Please enter a valid phone number"
      }
    }
  }
  
  export default validation
