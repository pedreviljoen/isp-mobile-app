
/**
 * Created by xl on 5/16/2018.
 */
import React from 'react';

import {createSwitchNavigator} from "react-navigation";

import { SignedOut, SignedIn } from "./";
import {UserDetails,VerifyCode} from "../sign-up"

  export const createRootNavigator = (signedIn,profileCompleted) => {

        var navigateTo = "";

        if( signedIn == true ){

            navigateTo = "SignedIn"

            if( profileCompleted == false){
               
                navigateTo = "UserDetails"
            }

        }
        else{

            navigateTo = "SignedOut"
        }

        return createSwitchNavigator(
            {
                SignedIn: SignedIn,
                SignedOut:SignedOut,  
                UserDetails:UserDetails,          
            },
            {
                initialRouteName:navigateTo
            }
        );
    
  };