/**
 * Created by rossd on 5/16/2018.
 */
import React from 'react';
import {View,Dimensions} from "react-native";
import {Welcome} from "../welcome";
import {Register,Login,UserDetails,VerifyCode} from "../sign-up"
import {createStackNavigator } from 'react-navigation';

const StackNavigatorOptions = {
    headerMode: "screen",
    cardStyle: {
        backgroundColor: "white",    
    },

};


export const SignedOut = createStackNavigator({
    Welcome:{
        screen:Welcome,
        navigationOptions:{
            header:null,       
        }
    },
    Register:{
        screen:Register,
        navigationOptions:{
            header:null,
        }
    },
    VerifyCode:{
        screen:VerifyCode,
        navigationOptions:{
            header:null,
        }
    },
},
{
    cardStyle: {
        backgroundColor: "white",    
    },
    headerMode: "screen",
    navigationOptions: ({navigation})=>{

      const {state} = navigation;

        return {
            initialRouteName: 'Welcome',
        }
    },
}

)

