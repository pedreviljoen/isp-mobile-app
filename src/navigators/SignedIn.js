/**
 * Created by xl on 25/05/2018.
 */
import React from 'react';
import {View,Dimensions} from "react-native";
import {Home,History,AccountSettings,NeedHelp} from "../home"
import {Survey,MultipleChoice,CheckBox,NumberInput,TextQuestion,TrueOrFalse,DateInput,CompleteSurvey,ImageInput} from "../survey"

import {SurveyHistory} from "../history"
import {createDrawerNavigator,createStackNavigator} from "react-navigation";
import { UserDetails } from '../sign-up/userDetails';
import { DrawerContent , DrawerButton,BackButton } from "../components";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

var {height, width} = Dimensions.get('window');

const DrawerNavigatorOptions={
  initialRouteName: 'HomeStack',
  drawerPosition:"left",
  drawerBackgroundColor:"#fd8343",
  contentComponent: props => <DrawerContent {...props}/>,
}



const HomeStack = createStackNavigator(
    {
      Home: {
        screen: Home,
      },
      Survey: {
        screen: Survey,
      },
      NumberInput: {
        screen: NumberInput,
      },
      TextQuestion: {
        screen: TextQuestion,
      },
      MultipleChoice: {
        screen: MultipleChoice,
      },
      CheckBox: {
        screen: CheckBox,
      },
      TrueOrFalse: {
        screen: TrueOrFalse,
      },
      ImageInput: {
        screen: ImageInput,
      },
      DateInput: {
        screen: DateInput,
      },
      CompleteSurvey: {
        screen: CompleteSurvey,
      },
    },
    {
        navigationOptions: ({navigation})=>{

          const {state} = navigation;

          var headerBrackets = state.params? state.params.headerBrackets:"";
          var headerTitle = state.params? state.params.headerTitle:"Surveys";
          var headerLeft =  state.params?<BackButton color={"white"} {...{navigation}}/> : <DrawerButton {...{navigation}}/>;

          if(state.params){
            if(state.params.routeName == "Home"){
              headerTitle ="Surveys";
              headerLeft =  <DrawerButton {...{navigation}}/>;
            }
          }     

        return {
        initialRouteName: 'Home',
        headerTitle: `${headerTitle}` + `${headerBrackets}`,
        headerMode:"screen",
        headerLeft:headerLeft,
        headerStyle:{backgroundColor:"#fd8343"},
        headerTitleStyle:{color:"white",alignSelf: 'center', flex: 1,textAlign: 'center', fontFamily: 'sans-serif-light',},
        titleStyle: {alignSelf: 'center', fontFamily: 'sans-serif-light',},
        headerRight:<View/>
        }},
    }
  );

  const HistoryStack = createStackNavigator(
    {
      History: {
        screen: History,
      },
      SurveyHistory: {
        screen: SurveyHistory,
      }
    },
    {
        navigationOptions: ({navigation})=> {

          const {state} = navigation;

          var headerTitle = state.params? state.params.headerTitle:"History";
          var headerLeft =  state.params?<BackButton color={"white"} {...{navigation}}/>:<DrawerButton {...{navigation}}/>;

          if(state.params){
            if(state.params.routeName == "History"){
              headerTitle ="History";
              headerLeft =  <DrawerButton {...{navigation}}/>;
            }
          }   
          
            return {
                initialRouteName: 'History',
                headerTitle: `${headerTitle}`,
                headerMode:"screen",
                headerLeft:headerLeft,
                headerStyle:{backgroundColor:"#fd8343"},
                headerTitleStyle:{color:"white",alignSelf: 'center', flex: 1,textAlign: 'center',},
                titleStyle: {alignSelf: 'center'},
                headerRight:<View />
            }
      },
    }
  );

  const AccountSettingsStack = createStackNavigator(
    {
        AccountSettings: {
        screen: AccountSettings,
      }
    },
    
    {
          cardStyle: {
            backgroundColor: "white",    
        },
        headerMode: "screen",
        navigationOptions: ({navigation})=>({
        initialRouteName: 'AccountSettings',
        headerTitle: 'Account Settings',
        headerMode:"screen",
        headerLeft:<DrawerButton {...{navigation}}/>,
        headerStyle:{backgroundColor:"#fd8343"},
        headerTitleStyle:{color:"white",alignSelf: 'center', flex: 1,textAlign: 'center',},
        titleStyle: {alignSelf: 'center'},
        headerRight:<View />
      }),
    }
  );

  // const NeedHelpStack = createStackNavigator(
  //   {
  //       NeedHelp: {
  //       screen: NeedHelp,
  //     }
  //   },
  //   {
  //       navigationOptions: ({navigation})=>({
  //       initialRouteName: 'NeedHelp',
  //       headerTitle: 'Need Help',
  //       headerMode:"screen",
  //       headerLeft:<DrawerButton {...{navigation}}/>,
  //       headerStyle:{backgroundColor:"#fd8343"},
  //       headerTitleStyle:{color:"white",alignSelf: 'center', flex: 1,textAlign: 'center',},
  //       titleStyle: {alignSelf: 'center'},
  //       headerRight:<View />
  //     }),
  //   }
  // );

export const SignedIn = createDrawerNavigator({
  HomeStack:{ 
      name:"HomeStack",
      screen:HomeStack,
      navigationOptions:{
        title:"Surveys",
        drawerIcon: <MaterialCommunityIcons size={height*0.037} color="rgba(255,255,255,0.7)" name='file-document-box'/>,
    }
   },
  HistoryStack:{
        name:"HistoryStack",
        screen:HistoryStack,
        navigationOptions:{
        title:"History",
        drawerIcon: <MaterialIcons size={height*0.037} color="rgba(255,255,255,0.9)" name='history'/>,      
      }
    },
  AccountSettingsStack:{
        name:"AccountSettingsStack",
        screen:AccountSettingsStack,
        navigationOptions:{
        title:"Account Settings",
        drawerIcon: <MaterialCommunityIcons size={height*0.037} color="rgba(255,255,255,0.9)" name='account-settings-variant'/>,      
      },
    },
}, DrawerNavigatorOptions)



