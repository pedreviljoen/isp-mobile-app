
import { NavigationActions , StackActions} from 'react-navigation'

export function resetTo(navigation,route) {

        const actionToDispatch = StackActions.reset({
            index: 0,
            key:null,
            actions: [NavigationActions.navigate({routeName: route})],
        });

        navigation.dispatch(actionToDispatch);

}


export function navigateTo(navigation,route,index,params) {

        const actionToDispatch = NavigationActions.navigate({
            index: index,
            key:index,
            actions: [NavigationActions.navigate({routeName: route},params)],
        });

        navigation.dispatch(actionToDispatch);
}

function getCurrentRoute({routes}){
    let route = routes[routes.length - 1];
    while (route.index !== undefined) route = route.routes[route.index];
    return route;
}