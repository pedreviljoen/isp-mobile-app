

export default function AnswerAction(answers, navigation, questions , survey, answer, buttonText) {


      var navigateTo = "";
      var headerBrackets = "";
      answers[Object.keys(questions)[(parseInt(navigation.getParam('nextQuestionKey', null)))]] = answer;

      if(buttonText == "Finish" || (parseInt(navigation.getParam('nextQuestionKey', null))+1) == Object.keys(questions).length ){
          navigateTo = "CompleteSurvey"
      }else{

          const type = questions[Object.keys(questions)[(parseInt(navigation.getParam('nextQuestionKey', null))+1)]].type;
              headerBrackets = " ("+(parseInt(navigation.getParam('nextQuestionKey', null))+2)+"/"+Object.keys(questions).length+")";

          if( type == "text"){

              navigateTo = "TextQuestion";
          }
          else if( type == "bool"){
  
              navigateTo = "TrueOrFalse"
          }
          else if( type == "number"){
  
              navigateTo = "NumberInput"
          }
          else if(type == "multiple"){
  
              navigateTo = "MultipleChoice"
          }
          else if(type == "date"){

              navigateTo = "DateInput"
          }
          else if(type == "video"){

            navigateTo = "VideoInput"
        }
        else if(type == "image"){

            navigateTo = "ImageInput"
        }
          else if(type == "checkbox"){
    
            navigateTo = "CheckBox"
        }
      }
               
      if(navigateTo !== ""){
          navigation.push(navigateTo, {
              userBalance:navigation.getParam('userBalance', null),
              reward:navigation.getParam('reward', null),
              answers:answers,
              surveyVersionID: navigation.getParam('surveyVersionID', null),
              userID:navigation.getParam('userID', null),
              phoneNumber:navigation.getParam('phoneNumber', null),
              nextQuestionKey: (parseInt(navigation.getParam('nextQuestionKey', null))+1),
              questionObjectLength:navigation.getParam('questionObjectLength', null),
              surveyId: navigation.getParam('surveyId', null),
              headerTitle: navigation.getParam('headerTitle', null),
              headerBrackets:headerBrackets,
            });
      }
     
}