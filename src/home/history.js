import React,{Component} from 'react';
import {FlatList,View,Dimensions,ActivityIndicator,Text} from "react-native";
import {ListItem} from "../components"
import AutoBind from "autobind-decorator";
import  ThemeColors  from "../components/Theme";
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'



var {height, width} = Dimensions.get('window');


export class History extends Component{

        state = {
            selected: (new Map(): Map<string, boolean>),
        };

        keyExtractor = (item, index) => item.id;


        onPressItem = (id:string,name:string,reward:float) => {

            const { firebase, auth , navigation, firestore, profile} = this.props;

            navigation.navigate("SurveyHistory", {
                surveyId: id,
                headerTitle:name,
                reward:reward,
                userID:auth.uid,
              });

        };

        getOrganisaton  = (id: string) => {

                //TODO:Need to get these organisations more efficiently
                var avatar = 'https://gracebyte.com/lattice/images/ss_1.jpg';

                if(this.props.organisations){
                    for (let organisation of this.props.organisations) {
                        if(organisation.id == id){
                            if(organisation.avatarUrl && organisation.avatarUrl !== "" ){
                                avatar = organisation.avatarUrl;
                            }
                         }
                    };
                }
                return avatar;
        };



        renderItem = ({item}) => {

             return(
                <ListItem
                id={item.id}
                name={item.name}
                reward={item.reward}
                isHome = {false}
                onPressItem={this.onPressItem}
                selected={!!this.state.selected.get(item.id)}
                title={item.name}
                description={item.description}
                avaterUrl={this.getOrganisaton(item.organisationID)}
                balance={item.reward}
                buttonText={"View"}
            />
            );

        };



        render() {

         const {history} = this.props

            var answeredSurveys = isLoaded( history)&&!isEmpty( history)?history:[]

            return (
                isLoaded(answeredSurveys)&&!isEmpty(answeredSurveys)?
                <View style={{flex :1}}>
                <FlatList
                data={answeredSurveys}
                extraData={this.state}
                keyExtractor={this.keyExtractor}
                renderItem={this.renderItem}
                />
                </View>
                :
                !isLoaded(answeredSurveys)&&answeredSurveys!==undefined?
                <View style={{flex: 1,justifyContent: 'center', alignItems: "center"}}><ActivityIndicator size="large" color="#fd8343"/></View>
                :
                <View style={{flex: 1,justifyContent: 'center',alignItems: "center",backgroundColor:"#fff"}}>
                    <Text style={{paddingBottom:10,width:"80%",textAlign: "center",color:"rgb(253, 131, 67)",fontSize:height*0.03}}>
                         No completed surveys found
                    </Text>
                    <Text  style={{width:"80%",textAlign: "center",fontSize:height*0.025,color:"rgba(253, 131, 67,0.5)"}}>
                        Philosophy of science without history of science is empty; history of science without philosophy of science is blind.
                        {"\n"}~Imre Lakatos
                    </Text>
                </View>
            );
       };
  };



  export default compose(
    firebaseConnect(),
    firestoreConnect(),
    connect(({ firebase: { auth , profile} }, props) => ({ auth , profile })),
    connect(({ firestore: { ordered } }, props) => ({users: ordered.users})),
    
    firestoreConnect((props) =>{ 
        return([
        { 
            collection: 'userAnswers',
            where: ['userID', '==', props.auth.uid],
            storeAs: "userAnswers"
        },
        ]);
     }),
    connect((state, props) =>
    {
        return{
           userAnswers:state.firestore.ordered.userAnswers,
           organisations:state.firestore.ordered.organisations,
        }
    }),

    firestoreConnect((props) =>{ 

        let firestoreArray=[]

        var userAnswers = props.userAnswers?props.userAnswers:[]

         userAnswers.forEach( (userAnswer) => {
          firestoreArray.push(     
          {
              collection: 'surveys',
              doc:userAnswer.surveyId,
              storeAs: "history"
          })
        });

        return firestoreArray
       }),

       connect((state, props) =>
       {
           return{
            history:state.firestore.ordered.history,
           }
       }),

  )(History);



