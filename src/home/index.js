import Home from "./home"
import AccountSettings from "./accountSettings"
import History from "./history"
import NeedHelp from "./needHelp"

export{
    Home,
    AccountSettings,
    History,
    NeedHelp,
}