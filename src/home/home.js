import React,{Component} from 'react';
import {FlatList,View,Dimensions,Text,ActivityIndicator} from "react-native";
import {ListItem} from "../components"
import AutoBind from "autobind-decorator";
import  ThemeColors  from "../components/Theme";
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'



var {height, width} = Dimensions.get('window');


export class Home extends Component{

        static contextTypes = {
            store: PropTypes.object.isRequired
        }

        componentWillMount () {

            const { firestore } = this.context.store
            firestore.get('surveys')
            firestore.get('userAnswers')
        }

        state = {
            selected: (new Map(): Map<string, boolean>),
            userAnswerTotalCount:null
        };

        keyExtractor = (item, index) => item.id;

        onPressItem = (id:string,name:string) => {

            const { firebase, auth , navigation, firestore, profile} = this.props;

            navigation.navigate("Survey", {
                surveyId: id,
                headerTitle: name,
                headerBrackets:"",
              });
        };

        getOrganisaton  = (id: string) => {

                // this is the standard image
                var avatar = 'https://ispani-a392c.firebaseapp.com/static/media/iSpani_Logo.ff402f83.png';
                if(this.props.organisations){
                    for (let organisation of this.props.organisations) {
                        if(organisation.id == id){
                            if(organisation.avatarUrl && organisation.avatarUrl !== "" ){
                                avatar = organisation.avatarUrl;
                            }
                         }
                    };
                }
                return avatar;
        };
 
        renderItem = ({item}) => {

            const {profile} = this.props;

            return(
                <ListItem
                id={item.id}
                name={item.name}
                counter = {item.completingTimesLeft}
                onPressItem={this.onPressItem}
                isHome = {true}
                selected={!!this.state.selected.get(item.id)}
                title={item.name}
                description={item.description}
                avaterUrl={this.getOrganisaton(item.organisationID)}
                balance={item.reward}
                buttonText={"Start"}
                 />               
            );

        };


        getSurveys  = () => {

            const {surveys,profile,userAnswers,auth} = this.props;
            var activeSurveys = [];
            var notDemoSurveys = [];
            var demoSurveys = [];


            if(this.state.userAnswerTotalCount == null && userAnswers){
                this.setState({userAnswerTotalCount:userAnswers.length})
            }

            if( surveys&&surveys.length > 0 && userAnswers && this.state.userAnswerTotalCount !== null && this.state.userAnswerTotalCount <= userAnswers.length){
                for (let survey of surveys) {
                    var userAnswerCount = 0;
                    for (let userAnswer of userAnswers) {
                        if(userAnswer.surveyId == survey.id && auth.uid == userAnswer.userID){
                            userAnswerCount++;
                        }
                    }
                    if(userAnswerCount <= survey.allowSurveyCompletionTimes || survey.allowSurveyCompletionTimes == undefined)
                    {
                        if(profile.role === "SuperAdmin" && survey.isActive == true && survey.isDemo == true){
                            survey.completingTimesLeft = survey.allowSurveyCompletionTimes-userAnswerCount
                            demoSurveys.push(survey)
                        }
                        else if(survey.isActive == true && survey.isDemo == false){
                            survey.completingTimesLeft = survey.allowSurveyCompletionTimes-userAnswerCount
                            notDemoSurveys.push(survey)
                        }
                    }
                };
            }

            if(demoSurveys.length > 0){
                activeSurveys = demoSurveys
            }
            else{
                activeSurveys = notDemoSurveys
            }

            return activeSurveys;
    };

        render() {

            var surveys = this.getSurveys()

            return (
                surveys&&surveys.length > 0 ?
                <View style={{flex :1}}>
                    <FlatList
                    data={surveys}
                    extraData={this.state}
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderItem}
                    />
                </View>
                :
                !isLoaded(surveys)?
                <View style={{flex: 1,justifyContent: 'center', alignItems: "center"}}><ActivityIndicator size="large" color="#fd8343"/></View>
                :
                <View style={{flex: 1,justifyContent: 'center',alignItems: "center",backgroundColor:"#fff"}}>
                    <Text style={{paddingBottom:10,width:"80%",textAlign: "center",color:"rgb(253, 131, 67)",fontSize:height*0.03}}>
                         No surveys found
                    </Text>
                    <Text  style={{width:"80%",textAlign: "center",fontSize:height*0.025,color:"rgba(253, 131, 67,0.5)"}}>
                        Keep checking, surveys will be coming soon
                    </Text>
                </View>

            );
       };
  };



  export default compose(
    firebaseConnect(),
    firestoreConnect(),
     connect(({ firebase: { auth , profile} }, props) => ({ auth , profile })),
     connect(({ firestore: { ordered } }, props) => ({ users: ordered.users })),

    firestoreConnect((props) =>{
            return([
                {
                    collection: 'surveys',
                    storeAs: "surveys"
                },
                { 
                    collection: 'organisations',
                    storeAs: "organisations"
                },
                { 
                    collection: 'userAnswers',
                    storeAs: "userAnswers"
                }
                ]);

     }),
     connect((state, props) =>
     {
         return{
            surveys:state.firestore.ordered.surveys,
            organisations:state.firestore.ordered.organisations,
            userAnswers:state.firestore.ordered.userAnswers,
         }
     }),

  )(Home);


