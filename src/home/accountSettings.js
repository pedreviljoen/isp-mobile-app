/**
 * Created by rossd on 5/16/2018.
 */
import React,{Component} from 'react';
import {View,StyleSheet,KeyboardAvoidingView,ScrollView,Dimensions,AppRegistry,Text,StatusBarIOS,PixelRatio,ActivityIndicator} from "react-native";
import {CountryTextField,BottomPageButton,Container,Header,Content,Footer,AppText,TextField,TextInput,EditDetailTextField} from "../components"
import AutoBind from "autobind-decorator";
import validate from '../sign-up/validateWrapper'
import  ThemeColors  from "../components/Theme";
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'

import CountryPicker, {getAllCountries} from 'react-native-country-picker-modal'

const SOUTH_AFRICA = [];


var {height, width} = Dimensions.get('window');

export class AccountSettings extends Component{

        state={
            country:this.props.profile.country,
            countryError:"",
            email:this.props.profile.email,
            emailError:"",
            firstName:this.props.profile.name,
            firstNameError:"",
            lastName:this.props.profile.surname,
            lastNameError:"",
            isLoading: false,
            error: "",
            editLastName:false,
            editCountry:false,
            editFirstName:false,
            editEmail:false,
            cca2:""
       
        }

    

    finishSignUp = () =>{

        
        const { firebase, auth , navigation, firestore, profile} = this.props;

        const {country,email,firstName,lastName} = this.state
        const countryError = validate('names',  country)
        const firstNameError = validate('names', firstName)
        const lastNameError = validate('names', lastName)
        const emailError = validate('username', email)
   
    
        this.setState({
          countryError: countryError,
          emailError:emailError,
          firstNameError:firstNameError,
          lastNameError:lastNameError,
          isLoading: true,
        })

        if (!emailError && ! countryError && !firstNameError &&!lastNameError &&!this.state.error ) {   
             

            firebase.auth().onAuthStateChanged((user) => {
                if (user) {

                    var currentTime = new Date().getTime()

                    let fbProfile ={
                       
                        balance:profile.balance,
                        avatarUrl:profile.avatarUrl,
                        country:country,
                        email:email,
                        name:firstName,
                        surname:lastName,
                        phoneNumber:profile.phoneNumber,
                        role:profile.role

                    }  

                    let userProfile ={
                        displayName:firstName+" "+lastName,
                        email:email,
                        photoUrl:"",
                    }   
                    
                    firebase.updateProfile(fbProfile).then(() => {
                        user.updateProfile(userProfile).then(() => {
                            this.setState({
                                editLastName:false,
                                editFirstName:false,
                                editCountry:false,
                                editEmail:false,
                                isLoading: false,
                            })
    
                            
                          }).catch((error) => {
                            this.setState({
                                editLastName:false,
                                editFirstName:false,
                                editCountry:false,
                                editEmail:false,
                                isLoading: false,
                                error:error.message,
                            })
                          });
                      }).catch((error) => {
                        this.setState({
                            editLastName:false,
                            editFirstName:false,
                            editCountry:false,
                            editEmail:false,
                            isLoading: false,
                            error:error.message,
                        })
                      });

                }                    
            }); 
            }
    }
    //TODO: move helper functions to a separate folder
    isThereAnError(errorText){
        if(typeof(errorText) == "undefined" || errorText == null || errorText == "" ){
           return false;
        }
        return true;
    }
     //TODO: move helper functions to a separate folder

    onChangeTextHandle = (errorText,value,errorName,name) =>{

        const {country,email,firstName,lastName} = this.state

        this.setState({[name]: value.trim()})

        if(!this.isThereAnError(errorText)){

            this.setState({
                [errorName] : validate(name,this.state[name])
            }) 

        }    
    }

    componentWillMount(){

        const { firebase, auth , navigation, firestore, profile} = this.props;

        const userCountryData = getAllCountries()
        .filter(country =>{ 
            return(this.props.profile.country == country.name.common)
          })
          this.setState({
            cca2: userCountryData[0].cca2,
          })
    }

    render(){

        return(
            <Container>
                <Header>
                    <View style={styles.registerHeader}>
                        <AppText h3>
                          Edit Profile Information
                        </AppText>
                    </View>
                </Header>
                <Content style={styles.content}>
                        <View style={[styles.removeBorder,{marginTop:height*0.01}]}>
                                    <EditDetailTextField  
                                        autoCapitalize={"sentences"}
                                        secureTextEntry={false}
                                        returnKeyType={"next"}
                                        keyboardType={"default"}
                                        onChangeText={value =>  this.onChangeTextHandle(validate('names', this.state.firstName),value,'firstNameError','firstName')}
                                        onBlur={() => {
                                            this.setState({
                                                firstNameError: validate('names', this.state.firstName)
                                            })
                                        }}
                                        onPress={() => {
                                            this.setState({
                                                editFirstName:true
                                            })
                                            this.setState({
                                                editLastName:false,
                                                editCountry:false,
                                                editEmail:false
                                            })
                                        }}
                                        autoFocus={this.state.editFirstName}
                                        value={this.state.firstName}
                                        editFirstName={this.state.editFirstName}
                                        error={this.state.firstNameError}
                                        label={"FIRSTNAME"}/>

                                </View> 
                                <View style={[styles.removeBorder,{marginTop:height*0.01}]}>
                                    <EditDetailTextField  
                                        autoCapitalize={"sentences"}
                                        secureTextEntry={false}
                                        returnKeyType={"next"}
                                        keyboardType={"default"}
                                        onChangeText={value =>  this.onChangeTextHandle(validate('names', this.state.lastName),value,'lastNameError','lastName')}
                                        onBlur={() => {
                                            this.setState({
                                                lastNameError: validate('names', this.state.lastName)
                                            })
                                        }}
                                        autoFocus={this.state.editLastName}
                                        value={this.state.lastName}
                                        editLastName={this.state.editLastName}
                                        onPress={() => {

                                            this.setState({
                                                editLastName:true
                                            })
                                            this.setState({
                                                editFirstName:false,
                                                editCountry:false,
                                                editEmail:false
                                            })
                                        }}
                                        error={this.state.lastNameError}
                                        label={"SURNAME"}/>


                                </View> 
                                <View style={[styles.removeBorder,{marginTop:height*0.01}]}>
                                    <EditDetailTextField 
                                        autoCapitalize={"none"}
                                        secureTextEntry={false}
                                        returnKeyType={"next"}
                                        keyboardType={"email-address"}
                                        onChangeText={value =>  this.onChangeTextHandle(validate('username', this.state.email),value,'emailError','email')}
                                        onBlur={() => {
                                            this.setState({
                                            emailError: validate('username', this.state.email)
                                            })
                                        }}
                                        onPress={() => {
                                            this.setState({
                                                editEmail:true
                                            })
                                            this.setState({
                                                editLastName:false,
                                                editFirstName:false,
                                                editCountry:false,
                                            })
                                        }}
                                        autoFocus={this.state.editEmail}
                                        value={this.state.email}
                                        editEmail={this.state.editEmail}
                                        error={this.state.emailError}
                                        label={"EMAIL ADDRESS"}/>

                                </View> 

                                <View style={[styles.removeBorder,{marginTop:height*0.01}]}> 
                                    {
                                    this.state.editCountry ?
                                    <View>
                                        <View style={[styles.textErrorInputViewStyles,{flexDirection:"row"},styles.removeBorder]}>
                                            <View style={{marginBottom:height*0.01}}>
                                                { typeof(this.state.countryError) == "undefined" || this.state.countryError == null || this.state.countryError == "" ?
                                                        <Text style={[styles.bigBlack,{textAlign:"left"}]}>COUNTRY<Text style={{ fontSize:15,fontFamily: 'sans-serif-light',color:"#939393"}}> (press flag)</Text></Text>
                                                        :
                                                        <Text style={[styles.bigBlack, ThemeColors.errorColor,{textAlign:"left"} ]}>COUNTRY</Text>
                                                }
                                            </View>
                                        </View>
                                        <View  style={[styles.textInputViewStyles,{flexDirection:"row"},styles.removeBorder]}>
                                            <View  style={{marginTop:7,marginRight:6}}> 
                                                    <CountryPicker
                                                    onChange={(value)=>{
                                                        this.onChangeTextHandle(validate('names', this.state.country),value.name,'countryError','country')
                                                        this.setState({
                                                            cca2: value.cca2,
                                                        })
                                                        }}
                                                    cca2={this.state.cca2}
                                                    translation='eng'
                                                    />
                                            </View>
                                            <View > 
                                                    <Text style={[styles.textInputStyles,{marginTop:10}]}>{this.state.country}</Text>
                                            </View>
                                        </View>
                                    </View>

                                    :   
                                    <EditDetailTextField  
                                        autoCapitalize={"sentences"}
                                        secureTextEntry={this.state.editDetails}
                                        returnKeyType={"next"}
                                        keyboardType={"default"}
                                        onChangeText={value =>  this.onChangeTextHandle(validate('names', this.state.country),value,'countryError','country')}
                                        onBlur={() => {
                                            this.setState({
                                                countryError: validate('names', this.state.country)
                                            })
                                        }}
                                        onPress={() => {
                                            this.setState({
                                                editCountry:true
                                            })
                                            this.setState({
                                                editLastName:false,
                                                editFirstName:false,
                                                editEmail:false
                                            })
                                        }}
                                        autoFocus={this.state.editCountry}
                                        value={this.state.country}
                                        editCountry={this.state.editCountry}
                                        error={this.state.countryError}
                                        label={"COUNTRY"}/>

                                    }

                                </View>  
                                    <View  style={ styles.removeBorder}> 
                                        {this.state.error ? 
                                        <Text style={[ThemeColors.errorColor,{ width: width * .8 },{ marginLeft: width * .1 }]} >{this.state.error}</Text>: null
                                        }
                                    </View> 

            
                </Content>
                <Footer>
                    <View style={styles.loginSection}>
                        { this.state.editLastName || this.state.editCountry || this.state.editFirstName || this.state.editEmail ?
                            <BottomPageButton loading={this.state.isLoading} text="Save" onPress={this.finishSignUp} />
                            :
                            null
                        }
                    </View>
                </Footer>
            </Container>

        )

    }

}

const styles=StyleSheet.create({
    textInputStyles:{
        color:"#939393",
        fontSize:15,
        fontFamily: 'sans-serif-light',
    }, 

    bigBlack: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 15,
        fontFamily: 'sans-serif-light',

    }, 
    textInputViewStyles:{
        height:height*.07,
        paddingLeft:15,
        marginBottom:10,
        width: width * .8,
        backgroundColor:"#d3d3d3",
        borderRadius:5

    }, 
    textErrorInputViewStyles:{
        width: width * .8,
    },

    buttonBar:{
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center"
    },
    registerHeader:{
        justifyContent:"center",
        alignItems:"center"
    },
    content:{
        justifyContent:"center",
        alignItems:"center"
    },
    removeBorder:{
        borderColor:'transparent',
    },
    loginButton:{
        backgroundColor:'rgba(0,0,0,0.9)'
    }
})

export default compose(
    firebaseConnect(),
    firestoreConnect(),
    connect(({ firebase: { auth , profile } }, props) => ({ auth , profile })),
    connect(({ firestore: { ordered } }, props) => ({users: ordered.users})),
  )(AccountSettings);