/**
 * Created by rossd on 5/16/2018.
 */
import React,{Component} from 'react';
import {View,StyleSheet,Text,KeyboardAvoidingView,ScrollView,Dimensions} from "react-native";
import {BottomPageButton,Container,Header,Content,Footer,AppText,TextField,TextInput} from "../components"
import AutoBind from "autobind-decorator";
import validate from '../sign-up/validateWrapper'
import  ThemeColors  from "../components/Theme";

import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'

var Intercom = require('react-native-intercom');
var {height, width} = Dimensions.get('window');

export class NeedHelp extends Component{


    state={
   
    }


    render(){
        
        return(
            <Container>
                <View style={{flex: 1,justifyContent: 'center',alignItems: "center",backgroundColor:"#fff"}}>
                    <Text style={{paddingBottom:10,width:"80%",textAlign: "center",color:"rgb(253, 131, 67)",fontSize:height*0.03}}>
                         No completed surveys found
                    </Text>
                    <Text  style={{width:"80%",textAlign: "center",fontSize:height*0.025,color:"rgba(253, 131, 67,0.5)"}}>
                        Philosophy of science without history of science is empty; history of science without philosophy of science is blind.
                        {"\n"}~Imre Lakatos
                    </Text>
                </View>
            </Container>
        )

    }

}

const styles=StyleSheet.create({

    buttonBar:{
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center"
    },
    registerHeader:{
        justifyContent:"center",
        alignItems:"center"
    },
    content:{
        justifyContent:"center",
        alignItems:"center"
    },
    removeBorder:{
        borderColor:'transparent',
    },
    loginButton:{
        backgroundColor:'rgba(0,0,0,0.9)'
    }
})

export default compose(
    firebaseConnect(),
    firestoreConnect(),
    connect(({ firebase: { auth , profile } }, props) => ({ auth, profile })),
    connect(({ firestore: { ordered } }, props) => ({users: ordered.users})),
  )(NeedHelp);