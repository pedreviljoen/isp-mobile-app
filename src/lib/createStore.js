
import { applyMiddleware, compose, createStore } from 'redux';
import { getFirebase, reactReduxFirebase} from 'react-redux-firebase';
import RNFirebase from 'react-native-firebase';
import { reduxFirestore } from 'redux-firestore'
import thunk from 'redux-thunk';
import {makeRootReducer} from './reducers';
import 'firebase/firestore'

const reactNativeFirebaseConfig = {
  debug: true,
};


const reduxFirebaseConfig = {
  userProfile: 'users' ,
  useFirestoreForProfile: true,
  enableRedirectHandling: false,
  enableLogging: false, 
  attachAuthIsReady: true, 
  firebaseStateName: 'firebase'
};

export default (initialState = { firebase: {} }) => {

  const middleware = [
    thunk.withExtraArgument({ getFirebase }),
  ];

  const store = createStore(
    makeRootReducer(),
    initialState, 
    compose(
     reactReduxFirebase( RNFirebase, reduxFirebaseConfig), 
     reduxFirestore(RNFirebase),
     applyMiddleware(...middleware)
    )
  )

  return store
}