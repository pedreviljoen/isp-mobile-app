/**
 * Created by rossd on 3/28/2018.
 */
import { combineReducers } from 'redux'
import { firebaseStateReducer } from 'react-redux-firebase'
import { firestoreReducer } from 'redux-firestore'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    // Add sync reducers here
    firebase: firebaseStateReducer,
    firestore: firestoreReducer,
    ...asyncReducers
  })
}

export default makeRootReducer

