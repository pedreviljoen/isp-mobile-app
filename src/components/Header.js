/**
 * Created by rossd on 5/16/2018.
 */
import React from "React";
import {View,StyleSheet,Dimensions} from "react-native";
import {PropTypes} from 'prop-types';


var {height, width} = Dimensions.get('window');
export const Header = (props)=>{

    const {children} = props
    return(
        <View style={styles.header}>
            {children}
        </View>
    )

}


Header.propTypes ={
    children: PropTypes.oneOfType([PropTypes.element,PropTypes.arrayOf(PropTypes.element)])
}



const styles = StyleSheet.create({

    header:{
       paddingTop:height*0.025,
        height:height*0.1,
        alignItems: "center"
    }
})