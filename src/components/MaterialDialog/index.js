import MaterialDialog from './MaterialDialog';
import MultiPickerMaterialDialog from './MultiPickerMaterialDialog';
import SinglePickerMaterialDialog from './SinglePickerMaterialDialog';

const Dialogs = {
  MaterialDialog,
  MultiPickerMaterialDialog,
  SinglePickerMaterialDialog,
};

module.exports = Dialogs;