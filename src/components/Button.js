/**
 * Created by rossd on 5/16/2018.
 */
import React from "React";
import {View,TouchableOpacity,StyleSheet,Text,Dimensions} from "react-native";
import {PropTypes} from 'prop-types';
var {height, width} = Dimensions.get('window');



export const Button = (props)=>{

    const {children,text,style,textColor,...other} = props;
    return(
        <TouchableOpacity {...other} >
            <View style={[styles.button,style]}>
                <Text style={[styles.text,textColor]}>
                    {text}{children}
                </Text>
            </View>
        </TouchableOpacity>
    )
}

Button.propTypes ={
    onPress: PropTypes.func,
    children: PropTypes.string
}

const styles = StyleSheet.create({
    button:{
        backgroundColor:"black",
        alignItems:"center",
        justifyContent:"center",
        padding:10,
        margin:10,
        width:100,
    },
    text:{
        fontFamily: 'sans-serif-light',
        color:"#fff",
        textAlign:"center",
        fontSize:height*0.03
    }
})
