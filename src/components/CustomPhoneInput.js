import React, { Component } from 'react';
import { StyleSheet, View,  Dimensions, Text } from 'react-native';
import  ThemeColors  from "./Theme";
import PhoneInput from 'react-native-phone-input';
import ModalPickerImage from './ModalPickerImage';
var {height, width} = Dimensions.get('window');
class CustomPhoneInput extends Component {
  constructor(props) {
    super(props);

    this.onPressFlag = this.onPressFlag.bind(this);
    this.selectCountry = this.selectCountry.bind(this);
    this.state = {
      pickerData: null,
    };
  }

  componentDidMount() {
    this.setState({
      pickerData: this.phone.getPickerData(),
    });
  }

  onPressFlag() {
   // this.myCountryPicker.open();
  }

  selectCountry(country) {
    this.phone.selectCountry(country.iso2);
  }

  render() {
    const {label,error,value,description,onChangePhoneNumber,onChangeText,onBlur,autoFocus} = this.props; 
    return (
      <View style={styles.container}>
        { typeof(error) == "undefined" || error == null || error == "" ?
            <Text style={styles.bigBlack}>{label}<Text style={styles.descriptionText}>{"  "+description}</Text></Text>
            :
            <Text style={[styles.bigBlack,ThemeColors.errorColor ]}>{label}<Text style={[styles.descriptionText,ThemeColors.errorColor ]}>{"  "+description}</Text></Text>
        }
             

        <PhoneInput
          ref={(ref) => {
            this.phone = ref;
          }}
          {...this.props}
          onPressFlag={this.onPressFlag}
          initialCountry={"za"}
          allowZeroAfterCountryCode={false}
          textProps={{onChangeText,onBlur,autoFocus}}
          style={{
                backgroundColor:"#d3d3d3",
                height:height*.07,
                marginBottom:10,
                paddingLeft:15,
                width: width * .8,
                borderRadius:5
          }}
          textStyle={{
            color:"#939393",
            fontSize:15,
        }}      
        />
        {error?
            <Text style={ThemeColors.errorColor} >{error}</Text>: null
            }

        <ModalPickerImage
          ref={(ref) => {
            this.myCountryPicker = ref;
          }}
          data={this.state.pickerData}
          onChange={(country) => {
            this.selectCountry(country);
          }}
          cancelText="Cancel"
        />
      </View>
    );
  }

}

let styles = StyleSheet.create({
    textInputStyles:{
        color:"#939393",
        fontSize:15,
        backgroundColor:ThemeColors.lightGray,
        height:40,
        marginBottom:10,
        paddingLeft:15,
        width: width * .8,
    },
    textStyles:{
        color:"#939393",
        fontSize:15,
    }, 
    bigBlack: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 15,
        fontFamily: 'sans-serif-light' 
    }, 
    descriptionText:{
        fontSize: 10,
        color: 'gray',
        paddingBottom:5,
        fontFamily: 'sans-serif-light' 
    },
    removeBorder:{
        borderColor:'transparent',
    }
});

module.exports = CustomPhoneInput;