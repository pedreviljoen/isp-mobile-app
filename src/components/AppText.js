/**
 * Created by rossd on 5/16/2018.
 */
import React from "React";
import {StyleSheet,Text} from "react-native";
import {PropTypes} from 'prop-types';



export const AppText = (props)=>{

    const {h1,h2,h3,children,style} = props;

    var textStyle;
    var text = children;

    if(h1){
        textStyle = styles.h1
        text = children
    }
    else if(h2){
        textStyle = styles.h2
        text =children
    }
    else if(h3){
        textStyle = styles.h3
        text = children
    }
    else{
        textStyle = styles.default
    }


    return(
        <Text style={[styles.base,textStyle,style]}>
            {text}
        </Text>
    )
}

Text.propTypes ={
    children: PropTypes.node
}

const styles = StyleSheet.create({

    base:{
        color:"#fd8343",
        fontFamily: 'sans-serif-light',
    },

    h1:{
        fontSize:40
    },
    h2:{
        fontSize:30

    },
    h3:{
        fontSize:20
    },
    default:{
        fontSize:10
    }

})


