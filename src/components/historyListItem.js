import React,{Component} from 'react';
import {FlatList,ScrollView,TouchableOpacity,View,Text,StyleSheet,Dimensions,Image} from "react-native";
import {BottomPageButton,Container,Header,Content,Footer,AppText,TextField,Button} from "../components"
import {Card, CardTitle, CardImage,CardContent, CardAction} from '../components/CardView';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import AutoBind from "autobind-decorator";
import  ThemeColors  from "../components/Theme";
import PropTypes from 'prop-types'


import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'



var {height, width} = Dimensions.get('window');

export class HistoryListItem extends React.PureComponent {
    
 

    render() {

    const { firebase, auth , navigation, firestore, profile} = this.props;

    const textColor = this.props.selected ? "red" : "black";

      return (
            <View style={styles.container}>
              <ScrollView>
                    <Card styles={{card: {width: width*0.93}}}>
                        <CardContent style={{alignItems:"left"}}>
                        <View style={{width:width*0.93,flexDirection:"row"}}>
                                <MaterialIcons style={{marginLeft:width*0.02,marginRight:width*0.02}} size={height*0.08} color="#fd8343" name='account-circle'/>
                                <Text style={[styles.title,{color:"#696969",paddingTop:height*0.014,marginLeft:width*0.02, width:width*0.52 , fontFamily: 'sans-serif-light'}]}>{this.props.phoneNumber}</Text>
                                <Text style={[styles.title,{ color:"#5cb85c",fontFamily: 'sans-serif-light',paddingTop:height*0.016,textAlign:"right"}]}>+ R {this.props.reward}</Text>                               
                        </View>
                        <View style={{width:width*0.93,flexDirection:"row",paddingLeft:width*0.2}}>
                            <Text style={[styles.title,{fontSize:height*0.08,textAlign:"right",fontFamily: 'sans-serif-light', fontSize:height*0.027,color:"#696969"}]}>Date Completed: </Text>
                            <Text style={[styles.title,{fontSize:height*0.08,textAlign:"right",fontFamily: 'sans-serif-light', fontSize:height*0.027,color:"#939393"}]}>{this.props.time.toLocaleDateString()}</Text> 
                        </View>
                        </CardContent>
                    </Card>
            </ScrollView>
            </View>  
      );
    }
  }


const styles = StyleSheet.create({


    container: {
        flex: 1,
        marginTop: 5,
        marginBottom: 5,
        alignItems:"center",
        justifyContent:"center"
      },
      title: {
        fontSize: height*0.03,
        backgroundColor: 'transparent',
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "rgba(0, 0, 0, 0.85)",
      },
      card: {
        width: 300
      },

})


  
