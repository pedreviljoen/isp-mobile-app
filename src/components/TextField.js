/**
 * Created by rossd on 5/17/2018.
 */

import React,{Component} from 'react';
import { View, StyleSheet, TextInput, Text, Dimensions } from 'react-native'
import AutogrowInput from 'react-native-autogrow-input';
import  ThemeColors  from "./Theme";
import PropTypes from 'prop-types'

var {height, width} = Dimensions.get('window');

export const TextField  = (props)=>{


    const {label,error,description,textArea} = props; 

    return(
        <View style={styles.removeBorder}>

           
            { typeof(error) == "undefined" || error == null || error == "" ?
                <Text style={styles.bigBlack}>{label}<Text style={styles.descriptionText}>{description}</Text></Text>
                        :
                        <Text style={[styles.bigBlack, ThemeColors.errorColor ]}>{label}<Text style={[styles.descriptionText,ThemeColors.errorColor ]}>{description}</Text></Text>
                    }
             
            <AutogrowInput  {...props}
                         defaultHeight={height*.08}
                        underlineColorAndroid={"transparent"}
                        style={styles.textInputStyles}            
                        />

            {error?
            <Text style={ThemeColors.errorColor} >{error}</Text>: null
            }

        </View>



    )

}



TextInput.propTypes={
    // style:PropTypes.any
}


const styles = StyleSheet.create({

    textInputStyles:{
        color:"#939393",
        fontSize:15,
        backgroundColor:"#d3d3d3",
        height:40,
        paddingLeft:15,
        marginBottom:10,
        width: width * .8,
        fontFamily: 'sans-serif-light',
        borderRadius:5
    }, 
    bigBlack: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 15,
        fontFamily: 'sans-serif-light',
    }, 
    descriptionText:{
        fontSize: 10,
        color: 'gray',
        paddingBottom:5,
        fontFamily: 'sans-serif-light',
    },
    removeBorder:{
        borderColor:'transparent',
    }

  })
