/**
 * Created by rossd on 5/16/2018.
 */
import React from "React";
import {View,TouchableOpacity,StyleSheet,Text,Dimensions,ActivityIndicator} from "react-native";
import {PropTypes} from 'prop-types';
import  ThemeColors  from "../components/Theme";


var {height, width} = Dimensions.get('window');
export const BottomPageButton = (props)=>{

    const {onPress,text,isSecondary,style,disabled,loading,color,spinnerColor} = props;
    const isLoading = (loading ? loading: false);

  
    const backGroundColor = (color ? color: "#fd8343");
    const spinColor = (spinnerColor ? spinnerColor: "#fdb48e");

    return(
            <TouchableOpacity  disabled={disabled} onPress={onPress} style={[isSecondary?styles.secondaryButton:styles.button,style?style:{},{backgroundColor: backGroundColor}]} >
                {!isLoading ? <Text style={isSecondary?styles.secondaryText:styles.text}>
                        {text}
                </Text>:
                <ActivityIndicator size="large" color={spinColor}/>
                }
            </TouchableOpacity>
    )
}

BottomPageButton.propTypes={
    text:PropTypes.string.isRequired,
    onPress:PropTypes.func.isRequired,
    isSecondary:PropTypes.bool,
    disabled:PropTypes.bool,
    color:PropTypes.string,
}

const styles = StyleSheet.create({
  text:{
    color:"white",
    fontSize:20,
    textAlign:"center",
    fontFamily: 'sans-serif-light'  
  },
  button:{
    width:width,
    height:height*0.08,
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#fd8343",
  },
  banner:{

  },

  secondaryText:{
    color:"#fff",
    fontSize:20,
    textAlign:"center",
    fontFamily: 'sans-serif-light'  
  },
  secondaryButton:{
    width:width,
    height:height*0.08,
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#fd8343",
    fontFamily: 'sans-serif-light'  
  }

})