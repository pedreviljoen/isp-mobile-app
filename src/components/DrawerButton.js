/**
 * Created by rossd on 4/2/2018.
 */
import React,{Component} from 'react';
import {TouchableOpacity,StyleSheet,Dimensions} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

var {height, width} = Dimensions.get('window');
export const DrawerButton = (navigation) =>{

       return (
            <TouchableOpacity
                style={styles.button}
                onPress={() => navigation.navigation.toggleDrawer()}
            >
                <MaterialCommunityIcons style={{ marginLeft:width*0.025}} size={height*0.05} color="white" name='menu'/>
            </TouchableOpacity>
        );
}

styles = StyleSheet.create({
    button:{
    marginLeft:width*0.05
    }
})


