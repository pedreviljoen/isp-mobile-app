/**
 * Created by rossd on 5/17/2018.
 */

import React,{Component} from 'react';
import { View, StyleSheet, TextInput, Text, Dimensions,TouchableOpacity } from 'react-native'
import AutogrowInput from 'react-native-autogrow-input';
import  ThemeColors  from "./Theme";
import PropTypes from 'prop-types'
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

var {height, width} = Dimensions.get('window');

export const EditDetailTextField  = (props)=>{


    const {autoCapitalize,secureTextEntry,keyboardType,onChangeText,onBlur,label,error,
        value,description,editLastName,editCountry,editFirstName,editEmail,onPress,autoFocus} = props; 

      var currentEditDetail = false;
        if(editLastName){
            currentEditDetail = true;
        }
        else if(editCountry){
            currentEditDetail = true;
        }
        else if(editFirstName){
            currentEditDetail = true;
        }
        else if(editEmail){
            currentEditDetail = true;
        }

    
    return(

        ( currentEditDetail ?

        <View style={styles.removeBorder}>

           
            { typeof(error) == "undefined" || error == null || error == "" ?
                <Text style={styles.bigBlack}>{label}<Text style={styles.descriptionText}>{description}</Text></Text>
                        :
                        <Text style={[styles.bigBlack, ThemeColors.errorColor ]}>{label}<Text style={[styles.descriptionText,ThemeColors.errorColor ]}>{description}</Text></Text>
                    }
                    <AutogrowInput  {...props}
                        defaultHeight={height*.07}
                        underlineColorAndroid={"transparent"}
                        style={styles.textInputStyles}            
                     />

            {error?
            <Text style={ThemeColors.errorColor} >{error}</Text>: null
            }  
        </View>
        :
        <View  style={styles.Border}>
            <Text style={styles.bigBlack}>{label}</Text>
            <View style={styles.profileTextStyle}>
                <Text style={styles.textStyles} >{value}</Text> 
                <TouchableOpacity onPress={onPress} style={styles.iconStyle}>  
                      <MaterialIcons   size={height*0.035} color="#696969" name='edit'/>    
                </TouchableOpacity>
            </View>
        </View>

        )

    )

}



TextInput.propTypes={
    // style:PropTypes.any
}


const styles = StyleSheet.create({

    iconStyle:{
        alignItems:"flex-end"
    },

    textInputStyles:{
        color:"#939393",
        fontSize:15,
        backgroundColor:"#d3d3d3",
        height:height*.07,
        paddingLeft:15,
        marginBottom:10,
        width: width * .8,
        paddingTop:height*.015,
        marginTop:height*.015,
        fontFamily: 'sans-serif-light',
        borderRadius:5
    }, 
    bigBlack: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 15,
        fontFamily: 'sans-serif-light',
    }, 
    descriptionText:{
        fontSize: 10,
        color: 'gray',
        paddingBottom:5,
        fontFamily: 'sans-serif-light',
    },
    removeBorder:{
        borderColor:'transparent',
    },
    Border:{
       
    },
    profileTextStyle:{
        backgroundColor:"#d3d3d3",
        height:height*.07,
        paddingLeft:15,
        width: width * .8,
        paddingTop:height*.015,
        marginTop:height*.015,
        flexDirection:"row",
        borderRadius:5
    },
    textStyles:{
        color:"#939393",
        fontSize:15,
        marginBottom:10,
        fontFamily: 'sans-serif-light',
        alignItems:"flex-start",
        width: width *.65,
    }, 

  })
