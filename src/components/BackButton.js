/**
 * Created by rossd on 4/2/2018.
 */
import React,{Component} from 'react';
import {TouchableOpacity,StyleSheet,Dimensions} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

var {height, width} = Dimensions.get('window');
export const BackButton = (navigation) =>{

       return (
            <TouchableOpacity
                style={styles.button}
                onPress={() => navigation.navigation.goBack()}
            >
                <MaterialIcons style={{ marginLeft:width*0.025}} size={height*0.05} color={navigation.color}  name='keyboard-backspace'/>
            </TouchableOpacity>
        );
}

styles = StyleSheet.create({
    button:{
    marginLeft:width*0.05
    }
})
