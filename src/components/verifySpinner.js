
import React,{Component} from 'react';
import {ActivityIndicator,StyleSheet,Dimensions,Text,iew} from "react-native";

var {height, width} = Dimensions.get('window');
export const VerifySpinner = (navigation) =>{

       return (
        <View style={{flex: 1,justifyContent: 'center', alignItems: "center"}}>
            <ActivityIndicator size="large" color="#fd8343"/>
            <Text style={styles.verifyText}>
                    Please wait while we auto-verify the sms code
            </Text>
        </View>
        );
}

styles = StyleSheet.create({
    verifyText:{
        paddingTop:height*0.02,
        fontSize:height*0.025,
        fontWeight: '100',
        fontFamily: 'sans-serif-light'  
    },
})



