/**
 * Created by rossd on 4/3/2018.
 */
import autobind from "autobind-decorator";
import React,{Component} from 'react';
import {StyleSheet,ScrollView,Text,TouchableOpacity,View,Dimensions} from "react-native";
import {DrawerItems, SafeAreaView} from "react-navigation"
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'

var Intercom = require('react-native-intercom');
 var {height, width} = Dimensions.get('window');


const DrawerContent = (props) => {

    
 const openIntercom = () =>{
    Intercom.displayMessenger();
 }

        return(

            <ScrollView>
                <SafeAreaView  forceInset={{ top: 'always', horizontal: 'never' }}>
                    <View style={styles.headerContainer}>
                      <MaterialIcons size={height*0.2} color="#fd8343" name='account-circle'/>
                        <View style={{flexDirection:"column",marginLeft:width*0.02,marginTop:width*0.04,maxWidth:width*0.5}}>
                            <View style={{flexDirection:'row',flexWrap: 'wrap',maxWidth:width*0.5}}>
                                <Text style={{color:"#fd8343",fontSize:height*0.025, fontFamily: 'sans-serif-light',paddingBottom:height*0.008,paddingTop:height*0.016}}>{props.profile.name} {props.profile.surname}</Text>
                            </View>
                            <View style={{flexDirection:'row',flexWrap: 'wrap',maxWidth:width*0.5}}>
                                <Text style={{color:"#939393",fontSize:height*0.02, fontFamily: 'sans-serif-light',paddingBottom:height*0.02}}>{props.profile.phoneNumber}</Text>
                            </View>
                            <View style={[styles.amountStyle,{flexDirection:'row',flexWrap: 'wrap',maxWidth:width*0.5}]}>
                                <Text style={{color:"white",fontSize:height*0.024,textAlign:"center"}}>R {props.profile.balance?props.profile.balance:"0.0"}</Text>                            
                            </View>
                        </View>
                      
                    </View>
                    <DrawerItems {...props} labelStyle={styles.menuLabel} itemStyle={styles.menuItem}/>
                    <View  style={styles.needHelpItem}>
                        <TouchableOpacity style={{flexDirection:'row',alignItems:"center"}} onPress={openIntercom}>
                            <MaterialIcons style={{paddingTop:height*0.01}} size={height*0.037} color="rgba(255,255,255,0.5)" name='help-outline'/>
                            <Text style={styles.needHelpLabel}>Need Help</Text>
                        </TouchableOpacity>
                    </View>
                </SafeAreaView>
            </ScrollView>
        )
}

const styles = StyleSheet.create({
    menuItem:{
        paddingLeft:width*0.03,
        borderColor:"white",
        borderBottomWidth:0.7,
        flex:1,
        height:height*0.08
    },
    menuLabel:{
        color:"white",
        fontSize:height*0.028,
        marginLeft:0,
    },
    needHelpLabel:{
        color:"white",
        fontWeight:"bold",
        fontSize:height*0.028,
        marginLeft:width*0.055,
        paddingTop:height*0.01,
    },
    needHelpItem:{
        paddingLeft:width*0.07,
        borderTopWidth:0,
        flexDirection:'row',
        borderColor:"white",
        borderBottomWidth:0.7,
        flex:1,
        height:height*0.07,
        marginLeft:0,
        paddingBottom:width*0.037,
        alignItems:"center"
    },
    headerContainer:{
        flexDirection:"row",
        padding:height*0.03,
        height:height*0.3,
        backgroundColor:"white",
        paddingLeft:height*0.015,
    },
    amountStyle:{
        borderWidth: 1,
        borderColor:"#fd8343",
        borderRadius:width*0.05,
        padding:height*0.008,
        marginLeft:width*0.02,
        backgroundColor:"rgba(253,131,67,0.8)",
        width:width*0.18,
        justifyContent:"center",
        alignItems:"center"

    }
})

export default compose(
    firebaseConnect(),
    firestoreConnect(),
    connect(({ firebase: { auth , profile } }, props) => ({ auth, profile })),
    connect(({ firestore: { ordered } }, props) => ({users: ordered.users})),
  )(DrawerContent)







