/**
 * Created by rossd on 5/16/2018.
 */
import React from "React";
import {View,StyleSheet,Dimensions,ScrollView} from "react-native";
import {PropTypes} from 'prop-types';


var {height, width} = Dimensions.get('window');
export const Content = (props)=>{

    const {children} = props
    return(
        <ScrollView centerContent={true} contentContainerStyle={{flexGrow: 1}}>
            <View style={styles.content}>         
                    {children} 
            </View>
         </ScrollView>
       

    )

}


Content.propTypes ={
    children: PropTypes.arrayOf(PropTypes.element)
}



const styles = StyleSheet.create({

    content:{
        height:height*0.6,
        alignItems: "center",
        justifyContent:"center",
    }
})