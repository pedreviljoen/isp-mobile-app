import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View,  Platform,Image,ActivityIndicator,Dimensions } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import { connect } from 'react-redux'
import { firebaseConnect,firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import RNFetchBlob from 'react-native-fetch-blob'
import firebase from 'firebase'
import FontAwesome from "react-native-vector-icons/FontAwesome";

// this is a temporal fix, the react-native-firebase library has a deprecated storage().putFile method with only takes path strings
// but firebase now expects a blob, when this is fixed please use RNFirebase so we dont need to link this new one, and update the
// rules in firebase storage not be public.
// firebase.initializeApp({
//     apiKey: "AIzaSyDIrSa_1_tiXDqMguTAr4g9sp6W5Il7G5U",
//     authDomain: "ispani-a392c.firebaseapp.com",
//     databaseURL: "https://ispani-a392c.firebaseio.com",
//     projectId: "ispani-a392c",
//     storageBucket: "ispani-a392c.appspot.com",
//     messagingSenderId: "884034585088"
//   });



var {height, width} = Dimensions.get('window');
const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob
console.disableYellowBox = true

export  class Video extends Component{
    constructor(props, context){
        super(props, context)
        this.state = {
          uploadURL:null
        }
    }

    
     uploadImage(uri, mime) {
      console.log(uri,mime)
      return new Promise((resolve, reject) => {
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri
        const sessionId = new Date().getTime()
        let uploadBlob = null
        const imageRef = this.props.firebase.storage().ref('images').child(`${sessionId}`)
    
        fs.readFile(uploadUri, 'base64')
          .then((data) => {
            return Blob.build(data, { type: `${mime};BASE64` })
          })
          .then((blob) => {
            uploadBlob = blob
            return imageRef.put(blob, { contentType: mime })
          })
          .then(() => {
            uploadBlob.close()
            return imageRef.getDownloadURL()
          })
          .then((url) => {
            resolve(url)
          })
          .catch((error) => {
            reject(error)
        })
      })
    }

    takeImage() {
      this.setState({ uploadURL: ''})
      ImagePicker.openCamera({
        width: 500,
        height: 600,
        cropping: true,
      }).then(image => {
        this.uploadImage(image.path,image.mime)
        .then(url => this.setState({ uploadURL: url }))
        .catch(error => console.log(error))
      });

    };

    pickImage() {
      this.setState({ uploadURL: '' })
      ImagePicker.openPicker({
        width: 500,
        height: 600,
        cropping: true
      }).then(image => {
        this.uploadImage(image.path,image.mime)
        .then(url => this.setState({ uploadURL: url }))
        .catch(error => console.log(error))
      });

    }

  render() {
  
    return (
      <View style={styles.container}>
      {
        (() => {
          switch (this.state.uploadURL) {
            case null:
              return <FontAwesome  size={height*0.2} color="rgba(253,131,67,0.9)" name='image'/>
            case '':
              return <ActivityIndicator  size="large" color={"rgba(253,131,67,0.9)"} />
            default:
              return (
                <View>
                  <Image
                    source={{ uri: this.state.uploadURL }}
                    style={ styles.image }
                  />
                </View>
              )
          }
        })()
      }
      <View>
        <Text style={{color:"#a6a6a6",marginTop:5,marginBottom:5,fontSize:width*0.04}}>
        {this.state.uploadURL == null?
          "Please add an image to answer this question"
          :this.state.uploadURL == ''?
            "Please wait while we load your image"
            :
            "Re-upload or press 'NEXT' if satisfied"}
        </Text>
        <View style={styles.buttons}>
          <TouchableOpacity style={[styles.actionButton,{marginRight:5}]}  onPress={ () => this.pickImage() }>
            <Text style={ styles.upload }>
              Upload Image
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.actionButton} onPress={ () => this.takeImage() }>
            <Text style={ styles.upload }>
              Take Image
            </Text>
          </TouchableOpacity>
        </View>    
        </View> 
      </View>    
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  actionButton:{
    backgroundColor:"#fff",    
    borderColor:"#fff",
    borderWidth: 1,
    borderRadius:5,
    shadowColor: '#D3D3D3',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 1,
    shadowRadius: 3,
    elevation: 1,
  },
  buttons:{
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection:"row",  
  },
  image: {
    width:width*0.9,
    height:width*0.8,
    resizeMode: 'contain',
  },
  upload: {
    textAlign: 'center',
    color: 'rgba(253,131,67,0.9)',
    padding: 10,
    marginBottom: 5,
    borderWidth: 1,
    borderColor:'white',
  },
});

export default compose(
    firebaseConnect(),
    firestoreConnect(),
    connect(({ firebase: { auth , profile } }, props) => ({ auth, profile })),
    connect(({ firestore: { ordered } }, props) => ({users: ordered.users})),
  )(Video)





