import React,{Component} from 'react';
import {FlatList,ScrollView,TouchableOpacity,View,Text,StyleSheet,Dimensions,Image} from "react-native";
import {BottomPageButton,Container,Header,Content,Footer,AppText,TextField,Button} from "../components"
import {Card, CardTitle, CardImage,CardContent, CardAction} from '../components/CardView';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import AutoBind from "autobind-decorator";
import  ThemeColors  from "../components/Theme";
import PropTypes from 'prop-types'


import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'



var {height, width} = Dimensions.get('window');

export class ListItem extends React.PureComponent {
    
 

    render() {

    const { firebase, auth , navigation, firestore, profile} = this.props;
    const textColor = this.props.selected ? "red" : "black";
      return (
            <View style={styles.container}>
              <ScrollView>
                    <Card styles={{card: {width: width*0.93,borderWidth: 1,
                        borderRadius: 2,
                        borderColor: '#ddd',
                        borderBottomWidth: 0,
                        shadowColor: '#000',
                        shadowOffset: { width: 0, height: 2 },
                        shadowOpacity: 0.8,
                        shadowRadius: 2,
                        elevation: 1}}}>
                        <CardTitle>
                            <View style={{width:width*0.93,flexDirection:"row"}}>
                                <MaterialIcons style={{marginLeft:width*0.02,marginRight:width*0.02}} size={height*0.08} color="#fd8343" name='account-circle'/>
                                <Text style={[styles.title,{paddingTop:height*0.014,marginLeft:width*0.02, width:width*0.52 , fontFamily: 'sans-serif-light'}]}>{this.props.title}</Text>
                                <Text style={[styles.title,{color:"#5cb85c", fontFamily: 'sans-serif-light',paddingTop:height*0.016,textAlign:"right"}]}>R {this.props.balance}</Text>
                            </View>
                        </CardTitle>
                        <CardContent style={{alignItems:"left"}}>
                            <View style={{width: width*0.93, height:width*0.5,alignItems:"center"}}><Image style={{width:width*0.93, height:width*0.5}} source={{uri:this.props.avaterUrl}}/></View>
                            <Text  style={{ color:"#696969",fontSize:height*0.027,fontFamily: 'sans-serif-light',paddingLeft:width*0.05,paddingTop:width*0.01}}>{this.props.description}</Text>
                            {this.props.isHome?
                            <Text  style={{color:"#fd8343",fontSize:height*0.027,fontFamily: 'sans-serif-light',paddingLeft:width*0.05,paddingBottom:width*0.02}}> Number of candidates to go: 
                                {'  '}<Text style={{fontStyle: "normal",fontWeight: "500", fontSize: height*0.03,color:"#5cb85c", fontFamily: 'sans-serif-light'}}>{this.props.counter?this.props.counter:0}</Text>
                            </Text>
                            :null
                            }
                        </CardContent>
                        <CardAction >
                            <BottomPageButton
                                text={this.props.buttonText}
                                style={styles.button}
                                onPress={() =>  this.props.onPressItem(this.props.id,this.props.name,this.props.reward)}
                                disabled={false}>
                            </BottomPageButton>
                        </CardAction>
                    </Card>
            </ScrollView>
            </View>  
      );
    }
  }


const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        marginTop: 5,
        marginBottom: 5,
        alignItems:"center",
        justifyContent:"center"
      },
      title: {
        fontSize: height*0.03,
        backgroundColor: 'transparent',
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "rgba(0, 0, 0, 0.85)",
      },
      card: {
        width: 300
      },

})


  
