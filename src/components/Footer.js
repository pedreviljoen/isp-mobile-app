/**
 * Created by rossd on 5/16/2018.
 */
import React from "React";
import {View,StyleSheet,Dimensions} from "react-native";
import {PropTypes} from 'prop-types';


var {height, width} = Dimensions.get('window')
export const Footer = (props)=>{

    const {children} = props
    
    return(
        <View style={styles.footer}>
            {children}
        </View>
    )

}


Footer.propTypes ={
    children: PropTypes.oneOfType([PropTypes.element,PropTypes.arrayOf(PropTypes.element)])

}



const styles = StyleSheet.create({

    footer:{
       height:height*0.08,
        justifyContent:"center",
        alignItems:"center",
    }
})