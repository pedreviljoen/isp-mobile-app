/**
 * Created by rossd on 5/17/2018.
 * Them
 * for more information please check https://www.madebymany.com/stories/a-year-of-react-native-styling-part-2
 */
import React from 'react';
import {StyleSheet,Dimensions} from "react-native";
var {height, width} = Dimensions.get('window');

//Add global colors here.....................................

export const mainColor = "#fd8343";
export const veryDarkGray = "#616161";
export const SunsetOrange = "#ff4c4c";
export const lightGreen = "#5cb85c"
export const lightGray = "#d3d3d3";
export const white = "#ffffff"

//End of global colors........................................

//Add global spacing

export const spacing20 = height*0.2;

//End of global spacing.......................................


export default StyleSheet.create({
    errorColor: { color: SunsetOrange },
    secondaryColor: { color:veryDarkGray },
    primaryColor:{ color: mainColor},
    successColor:{ color: lightGreen },
    warningColor:{ color: SunsetOrange },
    lightGray:{ color:lightGray},
    Spacing20:{marginTop:spacing20},
    mainButtonColor:{backgroundColor:mainColor}

});



