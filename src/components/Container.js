/**
 * Created by rossd on 5/16/2018.
 */
import React from "React";
import {View,StyleSheet,ScrollView,Dimensions,KeyboardAvoidingView} from "react-native";
import {PropTypes} from 'prop-types';


var {height, width} = Dimensions.get('window');
export const Container = (props)=>{

    const {children} = props

        return(
            <View style={styles.container}>
               <KeyboardAvoidingView keyboardVerticalOffset={0}>
                {children}
                </KeyboardAvoidingView>
            </View>

        )

}

Container.propTypes ={
    children: PropTypes.arrayOf(PropTypes.element),
    scroll: PropTypes.bool
}



const styles = StyleSheet.create({

    container:{
        flexShrink:1,
        justifyContent:"center",
        alignItems: "center",
    },

})
// <KeyboardAvoidingView keyboardVerticalOffset={height*0.12} behavior="padding" enabled>