import React,{Component} from 'react';
import {FlatList,View,Dimensions,ActivityIndicator} from "react-native";
import {HistoryListItem} from "../components"
import AutoBind from "autobind-decorator";
import  ThemeColors  from "../components/Theme";
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'



var {height, width} = Dimensions.get('window');

export class SurveyHistory extends Component{

        state = {
            selected: (new Map(): Map<string, boolean>),
            currentUserAnswers:[]
        };

        keyExtractor = (item, index) => item.id;


        onPressItem = (id:string,name:string,reward:float) => {

            // const { firebase, auth , navigation, firestore, profile} = this.props;


            // // navigation.navigate("SurveyHistory", {
            // //     surveyId: id,
            // //     headerTitle:name,
            // //     reward:reward,
            // //     userID:auth.uid,
            // //   });

        };

        getOrganisaton  = (id: string) => {

                //TODO:Need to get these organisations more efficiently
                var avatar = 'https://gracebyte.com/lattice/images/ss_1.jpg';

                if(this.props.organisations){
                    for (let organisation of this.props.organisations) {
                        if(organisation.id == id){
                            if(organisation.avatarUrl && organisation.avatarUrl !== "" ){
                                avatar = organisation.avatarUrl;
                            }
                         }
                    };
                }
                return avatar;
        };

        renderItem = ({item}) => {

            const {navigation} = this.props

             return(
                <HistoryListItem
                id={item.id}
                phoneNumber={item.phoneNumber}
                reward={navigation.getParam('reward', null)}
                time={item.timeCompleted}
                onPressItem={this.onPressItem}
                selected={!!this.state.selected.get(item.id)}
            />
            );

        };
        

        render() {

         const {userAnswers,navigation} = this.props
            
            const surveyID = navigation.getParam('surveyId', null);
            let currentUserAnswers = userAnswers.filter((currentUserAnswer) => {
                return currentUserAnswer.surveyId === surveyID;
            })

            return (
                 isLoaded(navigation) &  isLoaded(userAnswers)?
                <View style={{flex :1}}>
                <FlatList
                data={currentUserAnswers}
                extraData={this.state}
                keyExtractor={this.keyExtractor}
                renderItem={this.renderItem}
                />
                </View>
                :
                <View style={{flex: 1,justifyContent: 'center', alignItems: "center"}}><ActivityIndicator size="large" color="#fd8343"/></View>
            );
       };
  };


  export default compose(

    connect(({ firebase: { auth , profile} }, props) => ({ auth , profile })),
    firestoreConnect((props) =>{
        return([
        { 
            collection: 'userAnswers',
            where: [
                ['userID', '==', props.auth.uid],
                ['surveyId', '==', props.navigation.getParam('surveyId', null)]
            ],
            storeAs: "userAnswers"
        },
        ]);
     }),
    connect((state, props) =>
    {
        return{
           userAnswers:state.firestore.ordered.userAnswers,
        }
    }),

  )(SurveyHistory);



