import React,{Component} from 'react';
import {ScrollView,View,Text,StyleSheet,Dimensions} from "react-native";
import {BottomPageButton,Footer,ImageLoader} from "../components"
import {Card,CardTitle,CardContent,CardImage} from '../components/CardView';
import {AnswerAction} from "../actions"
import validate from '../sign-up/validateWrapper'


import { connect } from 'react-redux'
import { firestoreConnect,firebaseConnect } from 'react-redux-firebase'
import { compose } from 'redux'



var {height, width} = Dimensions.get('window');

export class ImageInput extends Component {

    state={
        answer:"",
        answerError:"",
        isLoading: false,
        error: {message:""},
        buttonText:"Next",
    }

     //TODO: move helper functions to a separate folder
    isThereAnError(errorText){
        if(typeof(errorText) == "undefined" || errorText == null || errorText == "" ){
           return false;
        }
        return true;
    }

     //TODO: move helper functions to a separate folder
     onChangeTextHandle = (errorText,value,errorName,name) => {

        this.setState({[name]: value})

        if(!this.isThereAnError(errorText)){

            this.setState({
                [errorName] : validate(name,this.state[name])
            }) 
        }
        
    }

    
    answer = () => {
        const {answer} = this.state
        const { navigation, questions , survey} = this.props;
        var answers = navigation.getParam('answers', null);
        const answerError = validate('answers',answer)
        this.setState({
            answerError: answerError,
        })

        if (!answerError && answer !== "" && answer != 0) {
            var answers = navigation.getParam('answers', null);
            AnswerAction(answers,navigation,questions,survey,answer,this.state.buttonText)
        }
    }

    componentWillMount = () => {

        const {navigation} = this.props;

        if((parseInt(navigation.getParam('nextQuestionKey', null))+1) == parseInt(navigation.getParam('questionObjectLength', null))){
            this.setState({
                buttonText: "Finish"
            })
        }

    }



    render() {
    const { navigation, questions } = this.props;

      return (
            <View style={styles.container}>
              <ScrollView>
                    <Card styles={{card: {paddingBottom:height*0.04,marginTop:height*0.2,width: width*0.93}}}>
                        <CardTitle>
                            <Text style={{ fontFamily: 'sans-serif-bold', fontSize:height*0.03,color:"#696969",width:width*0.8}}>{questions[Object.keys(questions)[navigation.getParam('nextQuestionKey', null)]].prompt}</Text>
                        </CardTitle>
                        <CardContent style={{alignItems:"left"}}>
                            <View>
                                {this.state.answerError ?
                                <Text style={{color:"#ff4c4c",marginBottom:width*0.3,marginLeft:width*0.1}}> 
                                    Please upload or capture an image
                                </Text>: null}
                            </View>
                        </CardContent>
                        <CardImage>
                        {true?<ImageLoader getImageLink={(link)=>{this.setState({answer:link,answerError:null})}} link ={new Date()}/>:null}
                        </CardImage>
                     </Card>
                </ScrollView>
                <Footer>
                    <View style={styles.loginSection}>
                        <BottomPageButton loading={this.state.isLoading} disabled={this.state.isLoading}  text={this.state.buttonText} onPress={this.answer} />
                    </View>
                </Footer>
            </View>
      );
    }
  }


const styles = StyleSheet.create({

    loginSection:{
        flex: 1,
        marginTop:height*0.015,
      },
    container: {
        flex: 1,
        marginTop: 5,
        marginBottom: 5,
        alignItems:"center",
        justifyContent:"center"
      },
      title: {
        fontSize: height*0.03,
        backgroundColor: 'transparent',
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "rgba(0, 0, 0, 0.85)",
      },
      card: {
        width: 300
      },

})
export default compose(
  firestoreConnect((props) =>{ 


    return([
    {
        collection: 'surveys',
        doc: props.navigation.getParam('surveyId', null),
        storeAs: "survey"
    },
    { collection: 'surveys',
        doc: props.navigation.getParam('surveyId', null),
        storeAs:"questions",
        subcollections:[{collection:"questions"}]
    },
    ])
 }),

 connect(({ firebase: { auth , profile} }, props) => ({ auth , profile })),
 connect((state, props) =>
 {
     return{
         survey: state.firestore.data.survey,
         questions:state.firestore.data.questions,
         usersSurveyed:state.firestore.data.usersSurveyed,
     }
 }),

)(ImageInput);