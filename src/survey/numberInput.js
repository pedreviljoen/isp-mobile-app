import React,{Component} from 'react';
import {FlatList,ScrollView,TouchableOpacity,View,Text,StyleSheet,Dimensions,Image,ActivityIndicator} from "react-native";
import {BottomPageButton,Container,Header,Content,Footer,AppText,TextField,Button} from "../components"
import {Card, CardTitle, CardImage,CardContent, CardAction} from '../components/CardView';
import {AnswerAction} from "../actions"
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import validate from '../sign-up/validateWrapper'
import AutoBind from "autobind-decorator";
import  ThemeColors  from "../components/Theme";
import PropTypes from 'prop-types'


import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'



var {height, width} = Dimensions.get('window');

export class NumberInput extends Component {

    state={
        answer:0,
        answerError:"",
        isLoading: false,
        error: {message:""},
        buttonText:"Next"
    }


    answer = () => {


        const {answer} = this.state
        const answerError = validate('number',answer)

        this.setState({
            answerError: answerError,
        })

        if (!answerError && answer !== "") {  

            const { navigation, questions , survey} = this.props;
            var answers = navigation.getParam('answers', null);   
    
            AnswerAction(answers,navigation,questions,survey,answer,this.state.buttonText) 
         
        }
    }

     //TODO: move helper functions to a separate folder
    isThereAnError(errorText){
        if(typeof(errorText) == "undefined" || errorText == null || errorText == "" ){
           return false;
        }
        return true;
    }

     //TODO: move helper functions to a separate folder
     onChangeTextHandle = (errorText,value,errorName,name) => {

        this.setState({[name]: value.trim()})

        if(!this.isThereAnError(errorText)){

            this.setState({
                [errorName] : validate(name,this.state[name])
            }) 
        }
        
    }

    
    componentWillMount = () => {

        const {navigation} = this.props;

        if(parseInt(navigation.getParam('nextQuestionKey', null)) == parseInt(navigation.getParam('questionObjectLength', null))){
            this.setState({
                buttonText: "Finish"
            })
        }

    }


    render() {

    const { navigation, questions , survey} = this.props;


      return (
            <View style={styles.container}>
              <ScrollView>
                    <Card styles={{card: {paddingBottom:height*0.04,marginTop:height*0.2,width: width*0.93}}}>
                        <CardTitle>
                            <Text style={{ fontFamily: 'sans-serif-bold', fontSize:height*0.03,color:"#696969",width:width*0.8}}>{questions[Object.keys(questions)[navigation.getParam('nextQuestionKey', null)]].prompt}</Text>
                        </CardTitle>
                        <CardContent style={{alignItems:"left"}}>
                        <View style={ styles.removeBorder}>
                                    <TextField  
                                        autoFocus={true}
                                        autoCapitalize={"sentences"}
                                        secureTextEntry={false}
                                        returnKeyType={"next"}
                                        keyboardType={"numeric"}
                                        onChangeText={value =>  this.onChangeTextHandle(validate('number', this.state.answer),value,'answerError','answer')}
                                        onBlur={() => {
                                            this.setState({
                                                answerError: validate('number', this.state.answer)
                                            })
                                        }}
                                        value={this.state.answer}
                                        error={this.state.answerError}
                                        label={""}/>

                                </View>  
                        </CardContent>
                     </Card>
                </ScrollView>
                <Footer>
                    <View style={styles.loginSection}>
                        <BottomPageButton loading={this.state.isLoading} disabled={this.state.isLoading}  text={this.state.buttonText} onPress={this.answer} />
                    </View>
                </Footer>
            </View>  
      );
    }
  }


const styles = StyleSheet.create({

    loginSection:{
        marginTop:height*0.015,
      },
    container: {
        flex: 1,
        marginTop: 5,
        marginBottom: 5,
        alignItems:"center",
        justifyContent:"center"
      },
      title: {
        fontSize: height*0.03,
        backgroundColor: 'transparent',
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "rgba(0, 0, 0, 0.85)",
      },
      card: {
        width: 300
      },

})
export default compose(

  firestoreConnect((props) =>{ 

      return([
      {
          collection: 'surveys',
          doc: props.navigation.getParam('surveyId', null),
          storeAs: "survey"
      },
      { collection: 'surveys',
          doc: props.navigation.getParam('surveyId', null),
          storeAs:"questions",
          subcollections:[{collection:"questions"}]
      },
      ])
   }),

   connect(({ firebase: { auth , profile} }, props) => ({ auth , profile })),
   connect((state, props) =>
   {
       return{
           survey: state.firestore.data.survey,
           questions:state.firestore.data.questions,
           usersSurveyed:state.firestore.data.usersSurveyed,
       }
   }),

)(NumberInput);
