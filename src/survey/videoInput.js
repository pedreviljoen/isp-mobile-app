import React,{Component} from 'react';
import {ScrollView,View,Text,StyleSheet,Dimensions} from "react-native";
import {BottomPageButton,Footer,Video} from "../components"
import {Card,CardTitle,CardContent} from '../components/CardView';
import {AnswerAction} from "../actions"
import validate from '../sign-up/validateWrapper'


import { connect } from 'react-redux'
import { firestoreConnect,firebaseConnect } from 'react-redux-firebase'
import { compose } from 'redux'



var {height, width} = Dimensions.get('window');

export class VideoInput extends Component {

    state={
        answer:0,
        answerError:"",
        isLoading: false,
        error: {message:""},
        buttonText:"Next"
    }

    answer = () => {

        const {answer} = this.state
       // const answerError = validate('number',answer)

        // this.setState({
        //     answerError: answerError,
        // })

        if (!answerError && answer !== "") {
            const { navigation, questions , survey} = this.props;
           // var answers = navigation.getParam('answers', null);

           // AnswerAction(answers,navigation,questions,survey,answer,this.state.buttonText)
        }
    }

    
    componentWillMount = () => {

        const {navigation} = this.props;

        // if(parseInt(navigation.getParam('nextQuestionKey', null)) == parseInt(navigation.getParam('questionObjectLength', null))){
        //     this.setState({
        //         buttonText: "Finish"
        //     })
        // }

    }


    render() {

    const { firebase} = this.props;

      return (
            <View style={styles.container}>
              <ScrollView>
                    <Card styles={{card: {paddingBottom:height*0.04,marginTop:height*0.2,width: width*0.93}}}>
                        <CardTitle>
                            <Text style={{ fontFamily: 'sans-serif-bold', fontSize:height*0.03,color:"#696969",width:width*0.8}}>{/*questions[Object.keys(questions)[navigation.getParam('nextQuestionKey', null)]].prompt*/"question"}</Text>
                        </CardTitle>
                        <CardContent style={{alignItems:"left"}}>
                            <View style={ styles.removeBorder}>
                                {true?<Video firebase = {firebase} link = {new Date()}/>:null}
                            </View>  
                        </CardContent>
                     </Card>
                </ScrollView>
                <Footer>
                    <View style={styles.loginSection}>
                        <BottomPageButton loading={this.state.isLoading} disabled={this.state.isLoading}  text={this.state.buttonText} onPress={this.answer} />
                    </View>
                </Footer>
            </View>  
      );
    }
  }


const styles = StyleSheet.create({

    loginSection:{
        marginTop:height*0.015,
      },
    container: {
        flex: 1,
        marginTop: 5,
        marginBottom: 5,
        alignItems:"center",
        justifyContent:"center"
      },
      title: {
        fontSize: height*0.03,
        backgroundColor: 'transparent',
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "rgba(0, 0, 0, 0.85)",
      },
      card: {
        width: 300
      },

})
export default compose(
  firebaseConnect(),
  connect(({ firebase: { auth , profile} }, props) => ({ auth , profile })),

)(VideoInput);