import React,{Component} from 'react';
import {FlatList,ScrollView,TouchableOpacity,View,Text,StyleSheet,Dimensions,Image,ActivityIndicator} from "react-native";
import {BottomPageButton,Container,Header,Content,Footer,AppText,TextField,Button} from "../components"
import {Card, CardTitle, CardImage,CardContent, CardAction} from '../components/CardView';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import validate from '../sign-up/validateWrapper'
import AutoBind from "autobind-decorator";
import  ThemeColors  from "../components/Theme";
import * as NavigationActions from "../actions/navigation"
import PropTypes from 'prop-types'


import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'



var {height, width} = Dimensions.get('window');

export class CompleteSurvey extends Component {


    
    state={
        isLoading: false,
        error: {message:""},
        latitude: null,
        longitude: null,
        geoError: null,
        text:"Please wait while we get your location"
    }


    
    save = (task) => {

        const {navigation, questions , firestore} = this.props;
        this.setState({
            isLoading:true,
        })


        if(navigation.getParam('answers', null) && questions  &&  navigation.getParam('surveyVersionID', null)  &&  
        navigation.getParam('userID', null) && navigation.getParam('phoneNumber', null)  && 
        navigation.getParam('surveyId', null)){

            if(this.state.geoError == null&&this.state.latitude !== null){

                const userAnswers = {
                    answers:navigation.getParam('answers', null),
                    questions:questions,
                    surveyVersionID: navigation.getParam('surveyVersionID', null),
                    userID:navigation.getParam('userID', null),
                    phoneNumber:navigation.getParam('phoneNumber', null),
                    surveyId: navigation.getParam('surveyId', null),
                    timeCompleted:firestore.FieldValue.serverTimestamp(),
                    geoLocation: new firestore.GeoPoint(this.state.latitude, this.state.longitude)
                }
                
                // check insurances survey
                /*
                if (true){
                    // invoke cloud function that sends twilio sms
                    someFunction = new Promise(params) {
                        resolve()
                        rejects()
                    }

                    someFunction(params).then(() => {
                        invoked on resovled
                    }).catch(error => {
                        invoked on reject
                    })

                    try {
                        const result = await someFunction()
                        
                    } catch (error) {
                        catches your error
                    }
                    // on success
                    return
                    // on fail
                    prompt error message
                    return
                }
                */
                        
                firestore.add({ collection: 'userAnswers' },userAnswers).then( () => {
                    firestore.get({
                        collection: 'userAnswers',
                        where: ['phoneNumber', '==', navigation.getParam('phoneNumber', null)],
                      }).then( (querySnapshot) => {
                            const userAnswerID = querySnapshot.docs[0]._ref._documentPath._parts[1];
                            const surveyedUser= {
                                [navigation.getParam('phoneNumber', null)]:userAnswerID,
                            };
                            const surveyID =navigation.getParam('surveyId', null);
    
                            firestore.set({ collection: 'usersSurveyed',doc:surveyID},surveyedUser,{ merge: true }).then( () => {
                                // refactor this to return decimal with negative reward
                                const newUserBalance = parseInt(navigation.getParam('userBalance', null)) + parseInt(navigation.getParam('reward', null));
                                firestore.update({ collection: 'users', doc: navigation.getParam('userID', null) }, {balance: newUserBalance}).then( () => {
                                    this.setState({
                                        isLoading: false,
                                    })
                                    NavigationActions.resetTo(navigation,"Home");
                                })
                             })
                      })
                }); 
    

            }
            else{
                this.setState({
                    isLoading: true,
                    text:this.state.geoError+".  Please try to submit again."
                  });
            }
            
        }
        else{
            this.setState({
                isLoading:true,
            })
        }
       
    }

    componentDidMount() {
        this.setState({
            isLoading: true,
          });
        navigator.geolocation.getCurrentPosition(
          (position) => {
            this.setState({
            isLoading: false,
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              geoError: null,
            });
          },
          (error) => this.setState({ 
               isLoading: false,
              geoError: error.message,
              text:error.message+". Please restart this survey or contact the admin."
             }),
          { enableHighAccuracy: false, timeout: 30000, maximumAge: 1000 },
        );
      }
    




    render() {

    const {questions} = this.props;

      return (
            <View style={styles.container}>
              <ScrollView>
                    <Card styles={{card: {width: width*0.93}}}>
                        <CardTitle>
                            <Text style={{textAlign:"center" ,fontFamily: 'sans-serif-bold', fontSize:height*0.03,color:"#696969",width:width*0.8}}>Survey Complete</Text>
                        </CardTitle>
                        <CardContent style={{alignItems:"left"}}>
                                {this.state.latitude==null||this.state.geoError!==null?
                                <Text style={{color:"rgba(253, 131, 67,0.7)",textAlign:"center", fontFamily: 'sans-serif-light', fontSize:height*0.027,width:width*0.8}}>{this.state.text}</Text>
                                :
                                <Text style={{textAlign:"center", fontFamily: 'sans-serif-light', fontSize:height*0.027,color:"rgb(253, 131, 67)",width:width*0.8}}>Well done for completing the survey</Text>}
                        </CardContent>
                        <CardAction >
                            <View style={styles.loginSection}>
                                <BottomPageButton  loading={this.state.isLoading} disabled={this.state.isLoading}  text={"Submit"} onPress={this.save} />
                            </View>
                        </CardAction>
                    </Card>
            </ScrollView>
            </View>  
      );
    }
  }


const styles = StyleSheet.create({


    container: {
        flex: 1,
        marginTop: 5,
        marginBottom: 5,
        alignItems:"center",
        justifyContent:"center"
      },
      title: {
        fontSize: height*0.03,
        backgroundColor: 'transparent',
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "rgba(0, 0, 0, 0.85)",
      },
      card: {
        width: 300
      },

})
export default compose(

    firestoreConnect((props) =>{ 
        return([
        { collection: 'surveys',
            doc: props.navigation.getParam('surveyId', null),
            subcollections:[{collection:"questions"}],
            storeAs:"questions",
        },
        ])
     }), 
    connect(({ firebase: { auth , profile} }) => ({ auth , profile })),

    connect((state) =>
    {
        return{
            questions:state.firestore.data.questions,
        }
    }),

)(CompleteSurvey);