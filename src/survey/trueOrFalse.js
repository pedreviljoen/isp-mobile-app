import React,{Component} from 'react';
import {FlatList,ScrollView,TouchableOpacity,View,Text,StyleSheet,Dimensions,Image,ActivityIndicator} from "react-native";
import {BottomPageButton,Container,Header,Content,Footer,AppText,TextField,Button} from "../components"
import {Card, CardTitle, CardImage,CardContent, CardAction} from '../components/CardView';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import validate from '../sign-up/validateWrapper'
import {AnswerAction} from "../actions"
import AutoBind from "autobind-decorator";
import  ThemeColors  from "../components/Theme";
import PropTypes from 'prop-types'


import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'



var {height, width} = Dimensions.get('window');

export class TrueOrFalse extends Component {


    state={
        answer:0,
        answerError:"",
        isLoading: false,
        error: {message:""},
        buttonText:"Next",
        iconTrueColor: "rgba(0,0,0,0.7)",
        iconFalseColor: "rgba(0,0,0,0.7)"
    }
    
    
     //TODO: move helper functions to a separate folder
    isThereAnError(errorText){
        if(typeof(errorText) == "undefined" || errorText == null || errorText == "" ){
           return false;
        }
        return true;
    }
    
     //TODO: move helper functions to a separate folder
     onChangeTextHandle = (errorText,value,errorName,name) => {
    
        this.setState({[name]: value.trim()})
    
        if(!this.isThereAnError(errorText)){
    
            this.setState({
                [errorName] : validate(name,this.state[name])
            }) 
        }
        
    }
    

 
    answer = () => {
  
  
        const {answer,answerError} = this.state
        const { navigation, questions , survey} = this.props;
        var answers = navigation.getParam('answers', null);

        if(answer === 0){

            this.setState({
                answerError:"Please select a choice",
            })
        }
        else{
            this.setState({
                answerError:"",
            })
        }

        if (answerError !== "Please select a choice"  && answer !== 0) {     

            const { navigation, questions , survey} = this.props;
            var answers = navigation.getParam('answers', null);   
    
            AnswerAction(answers,navigation,questions,survey,answer,this.state.buttonText) 
        }
    }

    onSelectTrue = () => {

        this.setState({
            answerError:"",
            iconTrueColor:"#70dd75",
            iconFalseColor:"rgba(0,0,0,0.7)",
            answer:true,
        })

    }

    onSelectFalse = () => {
        this.setState({
            answerError:"",
            iconFalseColor:"#ff4c4c",
            iconTrueColor:"rgba(0,0,0,0.7)",
            answer:false,
        })
    }


    componentWillMount = () => {

        const {navigation} = this.props;

        if(parseInt(navigation.getParam('nextQuestionKey', null)) == parseInt(navigation.getParam('questionObjectLength', null))){
            this.setState({
                buttonText: "Finish"
            })
        }

    }


    render() {

    
    const { firebase, auth , navigation, firestore, profile, questions , survey} = this.props;
   

      return (
            <View style={styles.container}>
                <ScrollView>
                        <Card styles={{card: {paddingBottom:height*0.04,paddingBottom:height*0.2,marginTop:height*0.2,width: width*0.93}}}>
                            <CardTitle>
                            <Text style={{ fontFamily: 'sans-serif-bold', fontSize:height*0.03,color:"#696969",width:width*0.8}}>{questions[Object.keys(questions)[navigation.getParam('nextQuestionKey', null)]].prompt}</Text>
                            </CardTitle>
                            <CardContent style={{alignItems:"left"}}>
                                <View style={{flexDirection:"row"}}>
                                        <TouchableOpacity onPress={this.onSelectTrue}style={{marginRight:20}}>
                                            <MaterialCommunityIcons size={height*0.13} color={this.state.iconTrueColor} name='emoticon-happy'/>
                                        </TouchableOpacity>
                                        <TouchableOpacity   onPress={this.onSelectFalse}>
                                            <MaterialCommunityIcons size={height*0.13} color={this.state.iconFalseColor} name='emoticon-sad'/>
                                        </TouchableOpacity>
                                </View>
                                <View>
                                    {this.state.answerError?
                                            <Text style={ThemeColors.errorColor} >{this.state.answerError}</Text>: <View/>
                                            }
                                </View>
                            </CardContent>
                            <CardAction >
                                <View style={styles.loginSection}>
                                    <BottomPageButton loading={this.state.isLoading} disabled={this.state.isLoading}  text={this.state.buttonText} onPress={this.answer} />
                                </View>
                            </CardAction>
                            </Card>
                </ScrollView>
                <Footer>
                    <View style={styles.loginSection}>
                        <BottomPageButton loading={this.state.isLoading} disabled={this.state.isLoading}  text={this.state.buttonText} onPress={this.answer} />
                    </View>
                </Footer>
            </View>  
      );
    }
  }


const styles = StyleSheet.create({

    loginSection:{
        marginTop:height*0.015,
      },
    container: {
        flex: 1,
        marginTop: 5,
        marginBottom: 5,
        alignItems:"center",
        justifyContent:"center"
      },
      title: {
        fontSize: height*0.03,
        backgroundColor: 'transparent',
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "rgba(0, 0, 0, 0.85)",
      },
      card: {
        width: 300
      },

})
export default compose(

  firestoreConnect((props) =>{ 


      return([
      {
          collection: 'surveys',
          doc: props.navigation.getParam('surveyId', null),
          storeAs: "survey"
      },
      { collection: 'surveys',
          doc: props.navigation.getParam('surveyId', null),
          storeAs:"questions",
          subcollections:[{collection:"questions"}]
      },
      ])
   }),

   connect(({ firebase: { auth , profile} }, props) => ({ auth , profile })),
   connect((state, props) =>
   {
       return{
           survey: state.firestore.data.survey,
           questions:state.firestore.data.questions,
           usersSurveyed:state.firestore.data.usersSurveyed,
       }
   }),

)(TrueOrFalse);
