import React,{Component} from 'react';
import {FlatList,ScrollView,TouchableOpacity,View,Text,StyleSheet,Dimensions,Image,Platform} from "react-native";
import {BottomPageButton,Container,Header,Content,Footer,AppText,TextField,Button} from "../components"
import {Card, CardTitle, CardImage,CardContent, CardAction} from '../components/CardView';
import CustomPhoneInput from '../components/CustomPhoneInput';
import { MaterialDialog } from '../components/MaterialDialog';
import validate from '../sign-up/validateWrapper'
import  ThemeColors  from "../components/Theme"
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import AutoBind from "autobind-decorator";
import PropTypes from 'prop-types'


import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty , firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'

var {height, width} = Dimensions.get('window');


export class Survey extends Component {

    state = {
        currentHeader:"Jonga Survey",
        questionLength:5,
        surveyID:this.props.navigation.getParam('surveyId', null),
        phoneNumber:"",
        phoneNumberError:"",
        isLoading:false,
        userCreatedSurvey:false,
        startSurvey:false,
        popupVisible:false,
        noQuestionsPopupVisible:false,
        error:"",
    };

    onChangeTextHandle = (errorText,value,errorName,name) => {

        const {phoneNumber} = this.state
       
        this.setState({[name]: value.trim()})

        if(!this.isThereAnError(errorText)){

            this.setState({
                [errorName] : validate(name,this.state[name])
            }) 

        }    
    }

    isThereAnError(errorText){
        if(typeof(errorText) == "undefined" || errorText == null || errorText == "" ){
            return false;
        }
        return true;
    }

    
    checkUser = () => {

        const {phoneNumber,error} = this.state

        const { firebase, auth , navigation, firestore, profile, usersSurveyed} = this.props;

        const phoneNumberError = validate('phoneNumber', phoneNumber)
    
        this.setState({
          phoneNumberError:phoneNumberError,
        })


        if (!phoneNumberError) {
            if(usersSurveyed && usersSurveyed[phoneNumber]){
                this.setState({
                    userCreatedSurvey: true,
                    startSurvey:false,
                })

                this.setState({ popupVisible: true })
            }
            else{
                this.setState({
                    userCreatedSurvey: false,
                    startSurvey:true,
                })
            }


        }

    };

    startSurvey = () => {

        const { navigation, questions, auth, survey,profile} = this.props;

        if(questions){

            const type = questions[Object.keys(questions)[0]].type;
            const userBalance = parseInt(profile.balance);
            const reward = parseInt(survey.reward);
            const surveyVersionID = survey.versionId;
            const userID = auth.uid;
            const nextQuestionKey= parseInt(0);
            const phoneNumber = this.state.phoneNumber;
            const questionObjectLength = Object.keys(questions).length;
            const surveyId = navigation.getParam('surveyId', null);
            const headerTitle = navigation.getParam('headerTitle', null);
            const headerBrackets = " ("+1+"/"+Object.keys(questions).length+")";
    
            var navigateTo = "";

            //TODO: this logic must be moved to actions folder
    
            if( type == "text"){
    
                navigateTo = "TextQuestion";
            }
            else if( type == "bool"){
    
                navigateTo = "TrueOrFalse"
            }
            else if( type == "number"){
    
                navigateTo = "NumberInput"
            }
            else if( type == "image"){
    
                navigateTo = "ImageInput"
            }
            else if(type == "multiple"){
    
                navigateTo = "MultipleChoice"
            }
            else if(type == "date"){
    
                navigateTo = "DateInput"
            }
            else if(type == "checkbox"){
    
                navigateTo = "CheckBox"
            }
    
    
            if(navigateTo !== ""){
                
                navigation.push(navigateTo, {
                    userBalance:userBalance,
                    reward:reward,
                    surveyVersionID:surveyVersionID,
                    userID:userID,
                    phoneNumber:phoneNumber,
                    answers:{},
                    nextQuestionKey:0,
                    questionObjectLength:questionObjectLength,
                    surveyId:surveyId,
                    headerTitle:headerTitle,
                    headerBrackets:headerBrackets
                  });
            }

        }
        else{
            this.setState({
                isLoading:false,
                noQuestionsPopupVisible: true
            })
        }
    };


 
    render() {

    const { firebase, auth , navigation, firestore, profile,  survey, questions, usersSurveyed} = this.props;


    const textColor = this.props.selected ? "red" : "black";

      return (        
            <View style={styles. containerStyle}>
                <View style={styles.contentContainer}>
                    <ScrollView>
                    { !this.state.startSurvey ?
                        <View style={styles.cardContainerStyle}>
                            <Card styles={{card:{height:height*0.5,width: width*0.93,flex:2}}}>
                                <View>
                                    <Text style={styles.cardTitle}>
                                        Okay, Let's Start
                                    </Text>
                                    <Text style={styles.cardSubText}>
                                        Let's quickly verify that your candidate has not completed this survey before by entering his/her cellphone number
                                    </Text>
                                </View>
                                <CardContent style={{alignItems:"left"}}>
                                <View style={styles.removeBorder}>
                                 <CustomPhoneInput
                                      autoFocus={true}
                                      ref='phone'
                                      onChangePhoneNumber={number =>  this.onChangeTextHandle(validate('phoneNumber', this.state.phoneNumber),number,'phoneNumberError','phoneNumber')}
                                      onBlur={() => {
                                            this.setState({
                                                phoneNumberError: validate('phoneNumber', this.state.phoneNumber)
                                            })
                                        }}
                                      value={this.state.phoneNumber}
                                      error={this.state.phoneNumberError}
                                      label={"PHONE NUMBER"}
                                      description={""}
                                      onChangeText={value =>  this.onChangeTextHandle(validate('phoneNumber', this.state.phoneNumber),value,'phoneNumberError','phoneNumber')}
                                      />

                                </View>  
                                    <View  style={ styles.removeBorder}> 
                                        {this.state.error ? 
                                        <Text style={[ThemeColors.errorColor,{ width: width * .8 },{ marginLeft: width * .1 }]} >{error}</Text>: null
                                        }
                                    </View> 
                                </CardContent>
                            </Card>
                        </View>
                            :
                            <Card styles={{card: {marginTop:height*0.2,width: width*0.93,flex:2}}}>
                                <View>
                                    <Text style={[styles.cardTitle,{textAlign:"center"}]}>
                                        We are ready to go
                                    </Text>
                                    <Text style={[styles.cardSubText,{textAlign:"center"}]}>
                                         This number is eligible to complete the survey. Press the start button to continue.
                                    </Text>
                                    <MaterialIcons style={{textAlign:"center",marginBottom:height*0.03,}} size={height*0.07} color={"#fd8343"}  name='arrow-downward'/>
                                </View>
                            </Card>
                    }
                    </ScrollView>
                         <Footer>
                         { !this.state.startSurvey ?
                            <View style={styles.loginSection}>
                                  <BottomPageButton loading={this.state.isLoading} disabled={this.state.isLoading}  text={"Verify User"} onPress={this.checkUser} />
                            </View>
                            :
                            <View style={styles.loginSection}>
                                <BottomPageButton loading={this.state.isLoading} disabled={this.state.isLoading}  text={"Start"} onPress={this.startSurvey} />
                            </View>
                         }
                        </Footer>
     
                        <MaterialDialog
                        title="Survey already Completed!"
                        visible={this.state.popupVisible}
                        titleColor={"#ff4c4c"}
                        okLabel={"OK"}
                        colorAccent={"#fff"}
                        onOk={() => this.setState({ popupVisible: false })}> 
                        <View  style={{ justifyContent:"center",alignItems:"center",width:width*0.8}}>                 
                            <Text style={{ marginLeft:width*0.07,fontFamily: 'sans-serif-light', fontSize:height*0.027,color:"#696969"}}>
                                Users cannot complete the same survey twice. Please try another survey.
                            </Text>
                        </View>
                        </MaterialDialog>
                        <MaterialDialog
                        title="Incomplete Survey"
                        visible={this.state.noQuestionsPopupVisible}
                        titleColor={"#ff4c4c"}
                        okLabel={"OK"}
                        colorAccent={"#fff"}
                        onOk={ () => {
                            this.setState({ noQuestionsPopupVisible: false })
                            navigation.goBack()
                        }}> 
                        <View  style={{ justifyContent:"center",alignItems:"center",width:width*0.8}}>                 
                            <Text style={{textAlign:"center",fontFamily: 'sans-serif-light', fontSize:height*0.027,color:"#696969"}}>
                                Please try another survey or contact the administrator in the need help tab.
                            </Text>
                        </View>
                        </MaterialDialog>
                </View>
        </View>    
      );
    }
  }


const styles = StyleSheet.create({

    containerStyle:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    },
    cardContainerStyle:{
        alignItems:"center",
        justifyContent:"center",
    },
    headerStyle:{
        flexGrow:1,
        alignItems:"center",
        height:height*0.18,
        width:width,
        backgroundColor:"#696969",
        flexDirection: "row",
        marginTop:Platform.OS == "ios" ? 20 : 0 ,
        paddingLeft:width*.05,
        fontFamily: 'sans-serif-light',
    },

    cardTitle:{
        fontStyle: "normal",
        color:"#fd8343",
        alignItems:"center",
        fontSize:height*0.045,
        fontFamily: 'sans-serif-light' ,
        width:width*0.8,
        marginTop:height*0.05,
    },
    cardSubText:{
        fontStyle: "normal",
        color:"#696969",
        alignItems:"center",
        fontSize:height*0.025,
        fontFamily: 'sans-serif-light',
        paddingTop:20,
        textAlign:"justify",
        width:width*0.8,
        marginBottom:height*0.03,
    },
    headerTextStyle:{
       fontSize:height*0.028,
       paddingTop:height*0.09,
       color:"#fff",
       color:"white",
       alignSelf: 'center',
        textAlign: 'center',
        fontFamily: 'sans-serif-light',
    },
    contentContainer: {
        flex:1,
        alignItems:"center",
        justifyContent:"center",
        marginTop: 5,
        marginBottom: 5,
      },
      title: {
        fontSize: height*0.03,
        backgroundColor: 'transparent',
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "rgba(0, 0, 0, 0.85)",
        fontFamily: 'sans-serif-light',
      },
      loginSection:{
        marginTop:height*0.015,
      }

})

export default compose(

        firestoreConnect((props) =>{ 
            return([
            {
                collection: 'surveys',
                doc: props.navigation.getParam('surveyId', null),
                storeAs: "survey"
            },
            { collection: 'surveys',
                doc: props.navigation.getParam('surveyId', null),
                subcollections:[{collection:"questions",orderBy:["index","asc"]}],
                storeAs:"questions",
            },
            { collection: 'usersSurveyed',
                doc: props.navigation.getParam('surveyId', null),
                storeAs: "usersSurveyed"
            },
            ])
         }),

         connect(({ firebase: { auth , profile} }, props) => ({ auth , profile })),
         
         connect((state, props) =>
         {
             return{
                 survey: state.firestore.data.survey,
                 questions:state.firestore.data.questions,
                 usersSurveyed:state.firestore.data.usersSurveyed
             }
         }),

    )(Survey);


